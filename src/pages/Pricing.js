/** @format */

import React from 'react';
import Layout from './Component/Layout';
import Ellipse from '../media/background/ellipse.png';

const Pricing = () => {
  return (
    <Layout>
      <div>
        <section className='page-banner banner banner-one'>
          <div className='container'>
            <div className='page-title-wrapper'>
              <h1 className='page-title'>Pricing Table</h1>
              <ul className='bradcurmed'>
                <li>
                  <a href='#' rel='noopener noreferrer'>
                    Home
                  </a>
                </li>
                <li> Pricing Table</li>
              </ul>
            </div>
            {/* /.page-title-wrapper */}
          </div>
          {/* /.container */}
          <svg
            className='circle'
            data-parallax='{"x" : -200}'
            xmlns='http://www.w3.org/2000/svg'
            xmlnsXlink='http://www.w3.org/1999/xlink'
            width='950px'
            height='950px'>
            <path
              fillRule='evenodd'
              stroke='rgb(250, 112, 112)'
              strokeWidth='100px'
              strokeLinecap='butt'
              strokeLinejoin='miter'
              opacity='0.051'
              fill='none'
              d='M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z'
            />
          </svg>
          <ul className='animate-ball'>
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
          </ul>
        </section>
        {/* /.page-banner */}
        {/*==============================*/}
        {/*=         Pricin One         =*/}
        {/*==============================*/}
        <section className='pricing-single-one'>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Pricing Plan</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                No Hidden Charges! Choose <br />
                your Plan.
              </h2>
            </div>
            {/* /.section-title */}
            <nav className='pricing-tab wow pixFadeUp' data-wow-delay='0.4s'>
              <span className='monthly_tab_title tab-btn'>Monthly</span>
              <span className='pricing-tab-switcher' />
              <span className='annual_tab_title tab-btn'>Annual</span>
            </nav>
            <div
              className='row advanced-pricing-table no-gutters wow pixFadeUp'
              data-wow-delay='0.5s'>
              <div className='col-lg-4'>
                <div className='pricing-table br-left'>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$0.00</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$0.00</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Basic Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='not'>Hotline Support 24/7</li>
                    <li className='not'>No Updates</li>
                  </ul>
                  <div className='action text-center'>
                    <a href='#' className='pix-btn btn-outline'>
                      Get Started
                    </a>
                  </div>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
              <div className='col-lg-4'>
                <div className='pricing-table color-two'>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$80.50</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$16.97</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Standard Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='have'>Hotline Support 24/7</li>
                    <li className='not'>No Updates</li>
                  </ul>
                  <div className='action text-center'>
                    <a href='#' className='pix-btn btn-outline'>
                      Get Started
                    </a>
                  </div>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
              <div className='col-lg-4'>
                <div className='pricing-table color-three'>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$180.70</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$29.45</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Premium Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='have'>Hotline Support 24/7</li>
                    <li className='have'>No Updates</li>
                  </ul>
                  <div className='action text-center'>
                    <a href='#' className='pix-btn btn-outline'>
                      Get Started
                    </a>
                  </div>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
            </div>
            {/* /.advanced-pricing-table */}
          </div>
          {/* /.container */}
        </section>
        {/* /.pricing */}
        {/*==================================*/}
        {/*=         Call to Action         =*/}
        {/*==================================*/}
        <section className='call-to-action action-padding'>
          <div className='overlay-bg'>
            <img src={Ellipse} alt='bg' />
          </div>
          <div className='container'>
            <div className='row align-items-center justify-content-between'>
              <div className='col-lg-7'>
                <div className='action-content style-two wow pixFadeUp'>
                  <h2 className='title'>
                    Getting connected with us
                    <br />
                    &amp; for the update.
                  </h2>
                </div>
                {/* /.action-content */}
              </div>
              {/* /.col-lg-7 */}
              <div className='col-lg-5 text-right'>
                <a
                  href='#newsletter'
                  className='pix-btn btn-light btn-big scroll-btn wow pixFadeUp'>
                  Subscribe Now!
                </a>
              </div>
              {/* /.col-lg-5 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </section>
        {/* /.call-to-action */}
        {/*==============================*/}
        {/*=         Pricin Two         =*/}
        {/*==============================*/}
        <section className='pricing-two-single'>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Pricing Plan</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                What’s our monthly pricing
                <br />
                subscription
              </h2>
            </div>
            {/* /.section-title */}
            <nav className='pricing-tab wow pixFadeUp' data-wow-delay='0.4s'>
              <span className='monthly_tab_title tab-btn'>Monthly</span>
              <span className='pricing-tab-switcher' />
              <span className='annual_tab_title tab-btn'>Annual</span>
            </nav>
            <div className='row advanced-pricing-table'>
              <div className='col-lg-4'>
                <div
                  className='pricing-table style-two wow pixFadeLeft'
                  data-wow-delay='0.5s'>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$0.00</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$0.00</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Basic Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='not'>Hotline Support 24/7</li>
                    <li className='not'>No Updates</li>
                  </ul>
                  <div className='action text-left'>
                    <a href='#' className='pix-btn btn-outline'>
                      Get Started
                    </a>
                  </div>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
              <div className='col-lg-4'>
                <div
                  className='pricing-table color-two style-two featured wow pixFadeLeft'
                  data-wow-delay='0.7s'>
                  <div className='trend'>
                    <p>Popular</p>
                  </div>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$80.50</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$16.97</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Standard Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='have'>Hotline Support 24/7</li>
                    <li className='not'>No Updates</li>
                  </ul>
                  <div className='action text-left'>
                    <a href='#' className='pix-btn btn-outline'>
                      Get Started
                    </a>
                  </div>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
              <div className='col-lg-4'>
                <div
                  className='pricing-table color-three style-two wow pixFadeLeft'
                  data-wow-delay='0.9s'>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$180.70</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$29.45</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Premium Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='have'>Hotline Support 24/7</li>
                    <li className='have'>No Updates</li>
                  </ul>
                  <div className='action text-left' />
                  <a href='#' className='pix-btn btn-outline'>
                    Get Started
                  </a>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
            </div>
            {/* /.advanced-pricing-table */}
          </div>
          {/* /.container */}
        </section>
      </div>
    </Layout>
  );
};

export default Pricing;
