/** @format */

import React from 'react';

import MainLogo from '../../assets/img/DH.png';
import StickyLogo from '../../assets/img/Dealer_Hero.png';

const Header = () => {
  return (
    <>
      <div id='home-version-1' class='home-version-4' data-style='default'>
        {/* <div> */}
        <a
          href='#main_content'
          data-type='section-switch'
          className='return-to-top'>
          <i className='fa fa-chevron-up' />
        </a>
        {/*
          <div className='page-loader'>
            <div className='loader'>
         
              <div className='blobs'>
                <div className='blob-center' />
                <div className='blob' />
                <div className='blob' />
                <div className='blob' />
                <div className='blob' />
                <div className='blob' />
                <div className='blob' />
              </div>
              <svg xmlns='http://www.w3.org/2000/svg' version='1.1'>
                <defs>
                  <filter id='goo'>
                    <feGaussianBlur
                      in='SourceGraphic'
                      stdDeviation={10}
                      result='blur'
                    />
                    <feColorMatrix
                      in='blur'
                      values='1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7'
                      result='goo'
                    />
                    <feBlend in='SourceGraphic' in2='goo' />
                  </filter>
                </defs>
              </svg>
            </div>
          </div> */}
        {/* </div> */}

        <div id='main_content'>
          <header className='site-header header_trans-fixed' data-top={992}>
            <div className='container'>
              <div className='header-inner'>
                <div className='toggle-menu'>
                  <span className='bar' />
                  <span className='bar' />
                  <span className='bar' />
                </div>
                {/* /.toggle-menu */}
                <div className='site-mobile-logo'>
                  <a href='/' className='logo'>
                    <img
                      // width='80%'
                      src={MainLogo}
                      alt='site logo'
                      className='main-logo'
                    />
                    <img
                      src={StickyLogo}
                      alt='site logo'
                      className='sticky-logo'
                    />
                  </a>
                </div>
                <nav className='site-nav'>
                  <div className='close-menu'>
                    <span>Close</span>
                    <i className='ei ei-icon_close' />
                  </div>
                  <div className='site-logo'>
                    <a href='/' className='logo'>
                      <img
                        src={MainLogo}
                        // width='80%'
                        alt='site logo'
                        className='main-logo'
                      />
                      <img
                        src={StickyLogo}
                        alt='site logo'
                        className='sticky-logo'
                      />
                    </a>
                  </div>
                  {/* /.site-logo */}
                  <div className='menu-wrapper' data-top={992}>
                    <ul className='site-main-menu'>
                      <li className='menu-item-has-children'>
                        <a href='/'>Home</a>
                        {/* <ul className='sub-menu'>
                          <li>
                            <a href='index.html'>Home One</a>
                          </li>
                          <li>
                            <a href='index-two.html'>Home Two</a>
                          </li>
                          <li>
                            <a href='index-three.html'>Home Three</a>
                          </li>
                          <li>
                            <a href='index-four.html'>Home Four</a>
                          </li>
                          <li>
                            <a href='index-five.html'>Home Five</a>
                          </li>
                          <li>
                            <a href='index-six.html'>Home Six</a>
                          </li>
                          <li>
                            <a href='index-seven.html'>Home Seven</a>
                          </li>
                          <li>
                            <a href='index-eight.html'>Home Eight</a>
                          </li>
                          <li>
                            <a href='index-nine.html'>Home Nine</a>
                          </li>
                          <li>
                            <a href='index-ten.html'>Home Ten</a>
                          </li>
                          <li>
                            <a href='index-eleven.html'>Home Eleven</a>
                          </li>
                        </ul> */}
                      </li>
                      <li>
                        <a href='/About'>About</a>
                      </li>
                      <li>
                        <a href='/Pricing'>Pricing</a>
                      </li>
                      <li className='menu-item-has-children'>
                        <a href='/Blog'>Blog</a>
                        {/* <ul className='sub-menu'>
                          <li>
                            <a href='blog.html'>Blog Standard</a>
                          </li>
                          <li>
                            <a href='blog-grid.html'>Blog Grid</a>
                          </li>
                          <li>
                            <a href='blog-single.html'>Blog Single</a>
                          </li>
                        </ul> */}
                      </li>
                      {/* <li className='menu-item-has-children'>
                        <a href='#'>Pages</a>
                        <ul className='sub-menu'>
                          <li>
                            <a href='about.html'>About</a>
                          </li>
                          <li>
                            <a href='service.html'>Service</a>
                          </li>
                          <li>
                            <a href='team.html'>Our Team</a>
                          </li>
                          <li>
                            <a href='pricing.html'>Pricing</a>
                          </li>
                          <li className='menu-item-has-children'>
                            <a href='portfolio.html'>Portfolio</a>
                            <ul className='sub-menu'>
                              <li>
                                <a href='portfolio-one.html'>Style One</a>
                              </li>
                              <li>
                                <a href='portfolio-two.html'>Style Two</a>
                              </li>
                              <li>
                                <a href='portfolio-three.html'>Style Three</a>
                              </li>
                              <li>
                                <a href='portfolio-single.html'>
                                  Portfolio Single
                                </a>
                              </li>
                            </ul>
                          </li>
                          <li>
                            <a href='faq.html'>Faq's</a>
                          </li>
                          <li>
                            <a href='error.html'>Error 404</a>
                          </li>
                          <li>
                            <a href='signin.html'>Sing In</a>
                          </li>
                          <li>
                            <a href='signup.html'>Sing Up</a>
                          </li>
                        </ul>
                      </li> */}
                      <li>
                        <a href='/Contact'>Contact</a>
                      </li>
                    </ul>
                    <div className='nav-right'>
                      <a
                        href='http://dadmin.dealershero.com/'
                        className='nav-btn'>
                       Dealer LogIn
                      </a>
                    </div>
                  </div>
                  {/* /.menu-wrapper */}
                </nav>
                {/* /.site-nav */}
              </div>
              {/* /.header-inner */}
            </div>
            {/* /.container */}
          </header>
        </div>
      </div>
    </>
  );
};

export default Header;
