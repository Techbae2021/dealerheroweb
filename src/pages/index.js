/** @format */

import * as React from 'react';
import Layout from './Component/Layout';
import Macbook from '../media/banner/macbook.png';
import Circle from '../media/banner/circle-shape.png';
import ShapeBg from '../media/banner/shape-bg.png';
import F1 from '../media/feature/1.png';
import F2 from '../media/feature/2.png';
import F3 from '../media/feature/3.png';
import F4 from '../media/feature/4.png';
import F5 from '../media/feature/41.png';
import F6 from '../media/background/shape_bg.png';
import F7 from '../media/feature/5.png';
import F8 from '../media/feature/51.png';
import F9 from '../media/background/shape.png';
import R1 from '../media/revolutionize/2.jpg';
import R2 from '../media/revolutionize/1.jpg';
import R3 from '../media/revolutionize/3.jpg';
import R4 from '../media/revolutionize/4.jpg';
import F10 from '../media/feature/6.png';
import F11 from '../media/feature/7.png';
import F12 from '../media/feature/8.png';
import F13 from '../media/feature/9.png';
import F14 from '../media/feature/10.png';
import F15 from '../media/feature/11.png';
import T1 from '../media/testimonial/1.jpg';
import Circle8 from '../media/background/circle8.png';
import Ellipse from '../media/background/ellipse.png';
import Circle13 from '../media/background/circle13.png';
import CB1 from '../chatBoot/images/default-color/sc2.png';
import CB2 from '../chatBoot/images/default-color/sc3.png';
import CB3 from '../chatBoot/images/default-color/sc5.png';
import CB4 from '../chatBoot/images/default-color/sc1.png';
import CB5 from '../chatBoot/images/default-color/sc4.png';
import CB6 from '../chatBoot/images/default-color/sc1.png';
import ChatBoat from '../chatBoot/images/default-color/chatBoat.png';
import Police from '../chatBoot/images/Police.png';
import Government from '../chatBoot/images/Government.png';
import Automated from '../chatBoot/images/Automated.png';
import BrokerDeals from '../chatBoot/images/Broker-Deals.png';
import Certificates from '../chatBoot/images/Certificates.png';
import Chat from '../chatBoot/images/Chat.png';
import Contactless from '../chatBoot/images/Contactless.png';
import Device from '../chatBoot/images/Device.png';
import Invoices from '../chatBoot/images/Invoices.png';
import Magic from '../chatBoot/images/Magic.png';
import Marketplace from '../chatBoot/images/Marketplace.png';
import PhotoApp from '../chatBoot/images/Photo-App.png';
import RentalVehicles from '../chatBoot/images/Rental-Vehicles.png';
import Reporting from '../chatBoot/images/Reporting.png';
import SaveTime from '../chatBoot/images/Save-Time.png';
import Security from '../chatBoot/images/Security.png';
import Support from '../chatBoot/images/Support.png';
import Users from '../chatBoot/images/Users.png';
import Vehicles from '../chatBoot/images/Vehicles.png';
import Websites from '../chatBoot/images/Websites.png';

// markup
const IndexPage = () => {
  return (
    <Layout>
      <div>
        <section className='banner banner-one'>
          <div className='circle-shape' data-parallax='{"y" : 230}'>
            <img src={Circle} alt='circle' />
          </div>
          <div className='container'>
            <div className='banner-content-wrap'>
              <div className='row align-items-center'>
                <div className='col-lg-6'>
                  <div className='banner-content'>
                    <h1
                      className='banner-title wow pixFadeUp'
                      data-wow-delay='0.3s'>
                      Saas{' '}
                      <span>
                        Software <br />
                        Landing
                      </span>{' '}
                      is the
                      <br />
                      Best Ever
                    </h1>
                    <p
                      className='description wow pixFadeUp'
                      data-wow-delay='0.5s'>
                      Why I say old chap that is spiffing bits and bobs chimney
                      <br />
                      pot cracking goal bamboozled.!
                    </p>
                    <a
                      href='http://dadmin.dealershero.com/signup'
                      className='pxs-btn banner-btn wow pixFadeUp'
                      data-wow-delay='0.6s'>
                      Get Free Trial
                    </a>
                  </div>
                  {/* /.banner-content */}
                </div>
                {/* /.col-lg-6 */}
                <div className='col-lg-6'>
                  <div className='promo-mockup wow pixFadeLeft'>
                    <img src={Macbook} alt='mpckup' />
                  </div>
                  {/* /.promo-mockup */}
                </div>
                {/* /.col-lg-6 */}
              </div>
              {/* /.row */}
            </div>
            {/* /.banner-content-wrap */}
          </div>
          {/* /.container */}
          <div className='bg-shape'>
            <img src={ShapeBg} alt='AAA' />
          </div>
        </section>

        <div id='main-wrapper' class='page-wrapper'>
          <div className='page-header section-padding full-height dc-seven'>
            <div className='container'>
              <div className='row align-items-center flex-row-reverse'>
                <div className='col-lg-6'>
                  <div className='image-wrapper' style={{ marginLeft: '25%' }}>
                    <img src={ChatBoat} alt />
                  </div>
                </div>
                {/* End Col */}
                <div className='col-lg-6'>
                  <div
                    className='heading-wrapper with-separator wow fadeInLeft'
                    data-wow-delay='0.2s'>
                    <h1>
                      AI Based chatbot for Connecting <span>Customers</span>
                    </h1>
                  </div>
                  <div
                    className='text-wrapper wow fadeInLeft'
                    data-wow-delay='0.4s'>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Aenean sodales dictum viverra. Nam gravida dignissim eros.
                      Vivamus congue erat ante, volutpat dictum neque dignissim
                      eget.
                    </p>
                  </div>
                  <div
                    className='btn-wrapper wow fadeInUp'
                    data-wow-delay='0.4s'>
                    <a className='btn btn-primary' href='#'>
                      Try it free Now
                    </a>
                  </div>
                </div>
                {/* End Col */}
              </div>
              {/* End Row */}
            </div>
          </div>
          {/* Page Header */}
          <div className='features-section section-padding pt-0'>
            <div className='container'>
              <div className='row clearfix justify-content-center'>
                <div className='col-lg-8'>
                  <div className='heading-wrapper with-separator text-center'>
                    <h2 className='h1'>
                      Countless <span>Key Features</span>
                    </h2>
                    <div className='lead-text'>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Duis finibus mi id elit gravida, quis tincidunt purus
                        fringilla. Aenean convallis a neque non pellentesque.
                      </p>
                    </div>
                  </div>
                  {/* End Heading */}
                  <div className='d-lg-none d-xl-block empty-space-30' />
                </div>
                {/* End Col */}
              </div>
              {/* End Row */}
              {/* <div className='row clearfix no-gutters dc-features-group'>
               
               
              </div> */}
            </div>
          </div>
        </div>

        <section className='featured-two'>
          <div className='container'>
            {/* <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Our service</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.2s'>
                Why you choose Our plugin
              </h2>
            </div> */}
            {/* /.section-title */}
            <div className='row'>
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={CB1} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Simplify Communication</a>
                    </h3>
                    <p>
                      Prints all required Government
                      <br />
                      Forms and complies with your state.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={CB2} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'> Drag and Drop Widgets</a>
                    </h3>
                    <p>
                      Prints all required Government
                      <br />
                      Forms and complies with your state.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={CB3} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Quick Setup</a>
                    </h3>
                    <p>
                      Prints all required Government
                      <br />
                      Forms and complies with your state.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={CB4} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Automatise Chatbots</a>
                    </h3>
                    <p>
                      Prints all required Government
                      <br />
                      Forms and complies with your state.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={CB5} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Automated Reminders</a>
                    </h3>
                    <p>
                      Prints all required Government
                      <br />
                      Forms and complies with your state.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={CB6} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Machine Learning</a>
                    </h3>
                    <p>
                      Prints all required Government
                      <br />
                      Forms and complies with your state.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </section>

        <section className='featured'>
          <div className='container'>
            <div className='section-title text-center wow pixFade'>
              <h3 className='sub-title'>Working Process</h3>
              <h2 className='title'>The only app you will need</h2>
            </div>
            {/* /.section-title */}
            <div className='row'>
              <div className='col-md-4'>
                <div
                  className='saaspik-icon-box-wrapper style-one wow pixFadeLeft'
                  data-wow-delay='0.3s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={F1} alt />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>
                        New sharing made for
                        <br /> people
                      </a>
                    </h3>
                  </div>
                </div>
                {/* /.pixsass-box style-one */}
              </div>
              {/* /.col-md-4 */}
              <div className='col-md-4'>
                <div
                  className='saaspik-icon-box-wrapper style-one wow pixFadeLeft'
                  data-wow-delay='0.5s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={F2} alt />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>
                        One integrated solution <br />
                        Manage
                      </a>
                    </h3>
                  </div>
                </div>
                {/* /.pixsass-box style-one */}
              </div>
              {/* /.col-md-4 */}
              <div className='col-md-4'>
                <div
                  className='saaspik-icon-box-wrapper style-one wow pixFadeLeft'
                  data-wow-delay='0.7s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={F3} alt />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>
                        Thousand of features and <br />
                        Custom option.
                      </a>
                    </h3>
                  </div>
                </div>
                {/* /.pixsass-box style-one */}
              </div>
              {/* /.col-md-4 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </section>
        {/* /.featured */}
        {/*=================================*/}
        {/*=         Editor Design         =*/}
        {/*=================================*/}
        <section className='editor-design'>
          <div className='container'>
            <div className='row'>
              <div className='editure-feature-image wow pixFadeRight'>
                <div className='image-one' data-parallax='{"x" : 30}'>
                  <img
                    src={F4}
                    className='wow pixFadeRight'
                    data-wow-delay='0.3s'
                    alt='feature-image'
                  />
                </div>
                <div className='image-two'>
                  <div className='image-two-inner' data-parallax='{"x" : -30}'>
                    <img
                      src={F5}
                      className='wow pixFadeLeft'
                      data-wow-delay='0.5s'
                      alt='feature-image'
                    />
                  </div>
                </div>
              </div>
              <div className='col-lg-6 offset-lg-6'>
                <div className='editor-content'>
                  <div className='section-title style-two'>
                    <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                      An editor designed
                      <br />
                      for contracts.
                    </h2>
                    <p className='wow pixFadeUp' data-wow-delay='0.5s'>
                      Having attractive showcase has never
                      <br />
                      been easier
                    </p>
                  </div>
                  <div
                    className='description wow pixFadeUp'
                    data-wow-delay='0.7s'>
                    <p>
                      The bee's knees off his nut cack it's all gone to pot
                      tinkety tonk old fruit blow off, tosser codswallop I
                      chinwag. Brilliant bobby haggle James Bond tickety-boo
                      horse play is spend a penny gutted mate absolutely.!
                    </p>
                    <a
                      href='#'
                      className='pix-btn wow pixFadeUp'
                      data-wow-delay='0.9s'>
                      Discover More
                    </a>
                  </div>
                </div>
                {/* /.editor-content */}
              </div>
              {/* /.col-lg-6 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
          <div className='shape-bg'>
            <img src={F6} className='wow fadeInLeft' alt='shape-bg' />
          </div>
        </section>
        {/* /.editor-design */}
        {/*===================================*/}
        {/*=         Genera Informes         =*/}
        {/*===================================*/}
        <section className='genera-informes'>
          <div className='container'>
            <div className='row'>
              <div className='col-lg-6 pix-order-one'>
                <div className='section-title style-two'>
                  <h2 className='title wow pixFadeUp'>
                    Genera informes <br />
                    completos con un solo
                  </h2>
                  <p className='wow pixFadeUp' data-wow-delay='0.3s'>
                    Burke blow off arse gutted mate what a plonker cup
                    <br /> of tea fantastic get stuffed mate.!
                  </p>
                </div>
                {/* /.section-title style-two */}
                <ul className='list-items wow pixFadeUp' data-wow-delay='0.4s'>
                  <li>Quick Access</li>
                  <li>Easily Manage</li>
                  <li>7/24h Support</li>
                </ul>
                <a
                  href='#'
                  className='pix-btn btn-outline wow pixFadeUp'
                  data-wow-delay='0.5s'>
                  Discover More
                </a>
              </div>
              {/* /.col-lg-6 */}
              <div className='informes-feature-image'>
                <div className='image-one' data-parallax='{"y" : 20}'>
                  <img src={F7} className='wow pixFadeDown' alt='informes' />
                </div>
                <div className='image-two' data-parallax='{"y" : -20}'>
                  <img
                    src={F8}
                    className=' mw-none wow pixFadeDown'
                    data-wow-delay='0.3s'
                    alt='informes'
                  />
                </div>
              </div>
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
          <div className='shape-bg'>
            <img src={F9} className='wow fadeInRight' alt='shape-bg' />
          </div>
        </section>
        {/* /.genera-informes */}
        {/*=================================*/}
        {/*=         Revolutionize         =*/}
        {/*=================================*/}
        <section className='revolutionize'>
          <div className='bg-angle' />
          <div className='container'>
            <div className='section-title dark-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Updated Screen</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                Revolutionize your online
                <br />
                business today
              </h2>
            </div>
            {/* /.section-title dark-title */}
            <div id='pix-tabs' className='wow pixFadeUp' data-wow-delay='0.5s'>
              <ul id='pix-tabs-nav'>
                <li>
                  <a href='#tab1'>Quick Access </a>
                </li>
                <li>
                  <a href='#tab1'>Easily Manage </a>
                </li>
                <li>
                  <a href='#tab1'>Data Transfer </a>
                </li>
                <li>
                  <a href='#tab1'>7/24h Support</a>
                </li>
              </ul>{' '}
              {/* tabs-nav */}
              <div id='pix-tabs-content'>
                <div id='tab1' className='content' style={{ display: 'block' }}>
                  <img src={R1} alt='revolutionize' />
                  <div className='shape-shadow' />
                </div>
                {/* <div id='tab2' className='content'>
                  <img src={R2} alt='revolutionize' />
                  <div className='shape-shadow' />
                </div>
                <div id='tab3' className='content'>
                  <img src={R3} alt='revolutionize' />
                  <div className='shape-shadow' />
                </div>
                <div id='tab4' className='content'>
                  <img src={R4} alt='revolutionize' />
                  <div className='shape-shadow' />
                </div> */}
              </div>{' '}
              {/* tabs-content */}
            </div>{' '}
            {/* Tabs */}
          </div>
          {/* /.container */}
        </section>
        {/* /.revolutionize */}
        {/*===========================*/}
        {/*=         Service         =*/}
        {/*===========================*/}
        <section className='featured-two'>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Our service</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.2s'>
                Why you choose Our plugin
              </h2>
            </div>
            {/* /.section-title */}
            <div className='row'>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img
                      // style={{ height: '150px', width: '185px' }}
                      src={Police}
                      alt='feature'
                    />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Police Book</a>
                    </h3>
                    <p>
                      Keeps a record of your dealings as
                      <br />
                      required by law.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Government} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Government Forms</a>
                    </h3>
                    <p>
                      Prints all required Government
                      <br />
                      Forms and complies with your state.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Invoices} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Invoices & Contracts</a>
                    </h3>
                    <p>
                      Our beautiful Invoices & Contracts
                      <br />
                      will complement your business.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Automated} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Automated Advertising</a>
                    </h3>
                    <p>
                      Your stock is sent automatically to
                      <br />
                      any advertiser you like.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Contactless} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Contactless Payments</a>
                    </h3>
                    <p>
                      Take deposits online from your
                      <br />
                      Virtual Yard Dealership Website or <br />
                      direct from your Tax Invoices.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={BrokerDeals} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Broker Deals</a>
                    </h3>
                    <p>
                      Sell vehicles you don't own.
                      <br />
                      Brokering a deal is easy with our <br />
                      Broker feature.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={RentalVehicles} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Rental Vehicles</a>
                    </h3>
                    <p>
                      Rent out your vehicles and create
                      <br />
                      Rental Agreements. Great for <br />
                      ridesharing vehicle providers.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Vehicles} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Wholesale Vehicles</a>
                    </h3>
                    <p>
                      Advertise your vehicles to other
                      <br />
                      Virtual Yard dealers.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Certificates} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>PPSR Certificates</a>
                    </h3>
                    <p>
                      Purchase a PPSR Certificate and it
                      <br />
                      automatically saves to the vehicle.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Websites} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Dealership Websites</a>
                    </h3>
                    <p>
                      100% Responsive Mobile friendly
                      <br />
                      websites showcasing your stock.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Marketplace} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Facebook Marketplace</a>
                    </h3>
                    <p>
                      Automatically advertise vehicles on
                      <br />
                      Facebook Marketplace.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={PhotoApp} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Photo App</a>
                    </h3>
                    <p>
                      Your stock in your hands, take
                      <br />
                      photos and reply to leads.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Users} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Unlimited Users</a>
                    </h3>
                    <p>
                      All your staff can use Virtual Yard
                      <br />
                      from any device.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Chat} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Live Chat</a>
                    </h3>
                    <p>
                      Live Chat available during business
                      <br />
                      hours 7 days per week right from <br />
                      within the software.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.4s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Device} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Any Device</a>
                    </h3>
                    <p>
                      Virtual Yard is cloud based and
                      <br />
                      Virtual Yard is cloud based and
                      <br />
                      web browser.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.5s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={SaveTime} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Save Time</a>
                    </h3>
                    <p>
                      We automatically print forms,
                      <br />
                      invoices and send your ads to your <br />
                      advertisers.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.6s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Magic} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Magic</a>
                    </h3>
                    <p>
                      We have built-in automated services
                      <br />
                      that make Virtual Yard magical. Like <br />
                      built-in PPSR certificates.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.7s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Reporting} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Reporting</a>
                    </h3>
                    <p>
                      Beautiful built-in dashboard and
                      <br />
                      reports on every facet of your <br />
                      business.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.8s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Security} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Security</a>
                    </h3>
                    <p>
                      We use industry standard SSL
                      <br />
                      encryption and perform regular <br />
                      backups to protect your data.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-md-3 col-sm-6 col-xs-12'>
                <div
                  className='saaspik-icon-box-wrapper style-two wow pixFadeRight'
                  data-wow-delay='0.9s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Support} alt='feature' />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>5 Star Support</a>
                    </h3>
                    <p>
                      Sends us an email and get a reply
                      <br />
                      within 2 hours. If it's urgent we call <br />
                      you straight back.
                    </p>
                  </div>
                </div>
                {/* /.pixsass-box style-two */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </section>
        {/* /.featured */}
        {/*===============================*/}
        {/*=         Testimonial         =*/}
        {/*===============================*/}
        <section className='testimonials swiper-init'>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Testiimonial</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                What our client say about us
              </h2>
            </div>
            {/* /.section-title */}
            <div
              className='testimonial-wrapper wow pixFadeUp'
              data-wow-delay='0.5s'>
              <div
                className='swiper-container'
                id='testimonial'
                data-speed={700}
                data-autoplay={5000}>
                <div className='swiper-wrapper'>
                  <div className='swiper-slide'>
                    <div className='testimonial'>
                      <div className='single-bio-thumb'>
                        <img src={T1} alt='testimonial' />
                      </div>
                      {/* /.single-bio-thumb */}
                      <div className='testimonial-content'>
                        <p>
                          Tosser nancy boy super tickety-boo lemon squeezy easy
                          peasy quaint, hunky-dory baking cakes pear shaped
                          butty do one, it's all gone to pot chinwag I cuppa
                          Eaton. Porkies amongst cockup absolutely bladdered
                          cobblers.!
                        </p>
                      </div>
                      {/* /.testimonial-content */}
                      <div className='bio-info'>
                        <h3 className='name'>Desmond Eagle</h3>
                        <span className='job'>Web designer</span>
                      </div>
                      {/* /.bio-info */}
                    </div>
                    {/* /.testimonial */}
                  </div>
                  <div className='swiper-slide'>
                    <div className='testimonial'>
                      <div className='single-bio-thumb'>
                        <img src={T1} alt='testimonial' />
                      </div>
                      {/* /.single-bio-thumb */}
                      <div className='testimonial-content'>
                        <p>
                          Tosser nancy boy super tickety-boo lemon squeezy easy
                          peasy quaint, hunky-dory baking cakes pear shaped
                          butty do one, it's all gone to pot chinwag I cuppa
                          Eaton. Porkies amongst cockup absolutely bladdered
                          cobblers.!
                        </p>
                      </div>
                      {/* /.testimonial-content */}
                      <div className='bio-info'>
                        <h3 className='name'>Desmond Eagle</h3>
                        <span className='job'>Web designer</span>
                      </div>
                      {/* /.bio-info */}
                    </div>
                    {/* /.testimonial */}
                  </div>
                  <div className='swiper-slide'>
                    <div className='testimonial'>
                      <div className='single-bio-thumb'>
                        <img src={T1} alt='testimonial' />
                      </div>
                      {/* /.single-bio-thumb */}
                      <div className='testimonial-content'>
                        <p>
                          Tosser nancy boy super tickety-boo lemon squeezy easy
                          peasy quaint, hunky-dory baking cakes pear shaped
                          butty do one, it's all gone to pot chinwag I cuppa
                          Eaton. Porkies amongst cockup absolutely bladdered
                          cobblers.!
                        </p>
                      </div>
                      {/* /.testimonial-content */}
                      <div className='bio-info'>
                        <h3 className='name'>Desmond Eagle</h3>
                        <span className='job'>Web designer</span>
                      </div>
                      {/* /.bio-info */}
                    </div>
                    {/* /.testimonial */}
                  </div>
                </div>
                {/* /.swiper-wrapper */}
              </div>
              {/* /.swiper-container */}
              <div className='shape-shadow' />
              <div className='slider-nav wow pixFadeUp' data-wow-delay='0.3s'>
                {/* <div id='slide-prev' className='swiper-button-prev'>
                  <i className='ei ei-arrow_left' />
                </div>
                <div id='slide-next' className=' swiper-button-next'>
                  <i className='ei ei-arrow_right' />
                </div> */}
              </div>
            </div>
            {/* /.testimonial-wrapper */}
          </div>
          {/* /.container */}
          <div className='scroll-circle wow pixFadeDown'>
            <img
              src='media/background/circle9.png'
              data-parallax='{"y" : 250}'
              alt='circle'
            />
          </div>
        </section>
        {/* /.testimonial */}
        {/*===========================*/}
        {/*=         Pricing         =*/}
        {/*===========================*/}
        <section className='pricing'>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Pricing Plan</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                No Hidden Charges! Choose <br />
                your Plan.
              </h2>
            </div>
            {/* /.section-title */}
            <nav className='pricing-tab wow pixFadeUp' data-wow-delay='0.4s'>
              <span className='tab-btn monthly_tab_title'>Monthly</span>
              <span className='pricing-tab-switcher' />
              <span className='tab-btn annual_tab_title'>Annual</span>
            </nav>
            <div
              className='row advanced-pricing-table no-gutters wow pixFadeUp'
              data-wow-delay='0.5s'>
              <div className='col-lg-4'>
                <div className='pricing-table br-left'>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$0.00</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$0.00</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Basic Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='not'>Hotline Support 24/7</li>
                    <li className='not'>No Updates</li>
                  </ul>
                  <div className='action text-center'>
                    <a href='#' className='pix-btn btn-outline'>
                      Get Started
                    </a>
                  </div>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
              <div className='col-lg-4'>
                <div className='pricing-table color-two'>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$80.50</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$16.97</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Standard Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='have'>Hotline Support 24/7</li>
                    <li className='not'>No Updates</li>
                  </ul>
                  <div className='action text-center'>
                    <a href='#' className='pix-btn btn-outline'>
                      Get Started
                    </a>
                  </div>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
              <div className='col-lg-4'>
                <div className='pricing-table color-three'>
                  <div className='pricing-header pricing-amount'>
                    <div className='annual_price'>
                      <h2 className='price'>$180.70</h2>
                    </div>
                    {/* /.annual_price */}
                    <div className='monthly_price'>
                      <h2 className='price'>$29.45</h2>
                    </div>
                    {/* /.monthly_price */}
                    <h3 className='price-title'>Premium Account</h3>
                    <p>Only for first month</p>
                  </div>
                  {/* /.pricing-header */}
                  <ul className='price-feture'>
                    <li className='have'>Limited Acess Library</li>
                    <li className='have'>Single User</li>
                    <li className='have'>eCommerce Store</li>
                    <li className='have'>Hotline Support 24/7</li>
                    <li className='have'>No Updates</li>
                  </ul>
                  <div className='action text-center'>
                    <a href='#' className='pix-btn btn-outline'>
                      Get Started
                    </a>
                  </div>
                </div>
                {/* /.pricing-table */}
              </div>
              {/* /.col-lg-4 */}
            </div>
            {/* /.advanced-pricing-table */}
          </div>
          {/* /.container */}
          <div className='faq-section'>
            <div className='container'>
              <div className='section-title text-center'>
                <h3 className='sub-title wow pixFadeUp'>
                  Frequently ask Question
                </h3>
                <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                  Want to ask something from us?
                </h2>
              </div>
              {/* /.section-title */}
              <div className='tabs-wrapper wow pixFadeUp' data-wow-delay='0.4s'>
                <ul className='nav faq-tabs' role='tablist'>
                  <li className='nav-item'>
                    <a
                      className='nav-link active'
                      id='design-tab'
                      data-toggle='tab'
                      href='#design'
                      role='tab'
                      aria-controls='design'
                      aria-selected='true'>
                      UI/UX Design
                    </a>
                  </li>
                  <li className='nav-item'>
                    <a
                      className='nav-link'
                      id='service-tab'
                      data-toggle='tab'
                      href='#service'
                      role='tab'
                      aria-controls='service'
                      aria-selected='false'>
                      Service
                    </a>
                  </li>
                  <li className='nav-item'>
                    <a
                      className='nav-link'
                      id='general-tab'
                      data-toggle='tab'
                      href='#general'
                      role='tab'
                      aria-controls='general'
                      aria-selected='false'>
                      General
                    </a>
                  </li>
                  <li className='nav-item'>
                    <a
                      className='nav-link'
                      id='branding-tab'
                      data-toggle='tab'
                      href='#branding'
                      role='tab'
                      aria-controls='branding'
                      aria-selected='false'>
                      Branding
                    </a>
                  </li>
                </ul>
                <div className='tab-content' id='myTabContent'>
                  <div
                    className='tab-pane fade show active'
                    id='design'
                    role='tabpanel'
                    aria-labelledby='design-tab'>
                    <div id='accordion' className='faq faq-two pixFade'>
                      <div className='card active'>
                        <div className='card-header' id='heading100'>
                          <h5>
                            <button
                              className='btn btn-link'
                              data-toggle='collapse'
                              data-target='#collapse001'
                              aria-expanded='false'
                              aria-controls='collapse001'>
                              How to contact with Customer Service?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse001'
                          className='collapse show'
                          aria-labelledby='heading100'
                          data-parent='#accordion'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading200'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse100'
                              aria-expanded='true'
                              aria-controls='collapse100'>
                              How delete my account?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse100'
                          className='collapse'
                          aria-labelledby='heading200'
                          data-parent='#accordion'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading300'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse200'
                              aria-expanded='false'
                              aria-controls='collapse200'>
                              Where is the edit optioon on dashboard?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse200'
                          className='collapse'
                          aria-labelledby='heading300'
                          data-parent='#accordion'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading400'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse300'
                              aria-expanded='false'
                              aria-controls='collapse300'>
                              Is there any custome pricing system?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse300'
                          className='collapse'
                          aria-labelledby='heading400'
                          data-parent='#accordion'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className='tab-pane fade'
                    id='service'
                    role='tabpanel'
                    aria-labelledby='service-tab'>
                    <div id='accordion-2' className='faq faq-two pixFade'>
                      <div className='card active'>
                        <div className='card-header' id='heading101'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse01'
                              aria-expanded='false'
                              aria-controls='collapse01'>
                              How to contact with Customer Service?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse01'
                          className='collapse show'
                          aria-labelledby='heading101'
                          data-parent='#accordion-2'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading201'>
                          <h5>
                            <button
                              className='btn btn-link'
                              data-toggle='collapse'
                              data-target='#collapse101'
                              aria-expanded='true'
                              aria-controls='collapse101'>
                              How delete my account?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse101'
                          className='collapse'
                          aria-labelledby='heading201'
                          data-parent='#accordion-2'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading301'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse201'
                              aria-expanded='false'
                              aria-controls='collapse201'>
                              Where is the edit optioon on dashboard?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse201'
                          className='collapse'
                          aria-labelledby='heading301'
                          data-parent='#accordion-2'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading401'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse301'
                              aria-expanded='false'
                              aria-controls='collapse301'>
                              Is there any custome pricing system?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse301'
                          className='collapse'
                          aria-labelledby='heading401'
                          data-parent='#accordion-2'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className='tab-pane fade'
                    id='general'
                    role='tabpanel'
                    aria-labelledby='general-tab'>
                    <div id='accordion-3' className='faq faq-two pixFade'>
                      <div className='card active'>
                        <div className='card-header' id='heading102'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse002'
                              aria-expanded='false'
                              aria-controls='collapse002'>
                              How to contact with Customer Service?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse002'
                          className='collapse show'
                          aria-labelledby='heading102'
                          data-parent='#accordion-3'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading202'>
                          <h5>
                            <button
                              className='btn btn-link'
                              data-toggle='collapse'
                              data-target='#collapse102'
                              aria-expanded='true'
                              aria-controls='collapse102'>
                              How delete my account?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse102'
                          className='collapse'
                          aria-labelledby='heading202'
                          data-parent='#accordion-3'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading303'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse203'
                              aria-expanded='false'
                              aria-controls='collapse203'>
                              Where is the edit optioon on dashboard?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse203'
                          className='collapse'
                          aria-labelledby='heading303'
                          data-parent='#accordion-3'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading403'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse303'
                              aria-expanded='false'
                              aria-controls='collapse303'>
                              Is there any custome pricing system?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse303'
                          className='collapse'
                          aria-labelledby='heading403'
                          data-parent='#accordion-3'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className='tab-pane fade'
                    id='branding'
                    role='tabpanel'
                    aria-labelledby='branding-tab'>
                    <div id='accordion-4' className='faq faq-two pixFade'>
                      <div className='card active'>
                        <div className='card-header' id='heading10'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse00'
                              aria-expanded='false'
                              aria-controls='collapse00'>
                              How to contact with Customer Service?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse00'
                          className='collapse show'
                          aria-labelledby='heading10'
                          data-parent='#accordion-4'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading20'>
                          <h5>
                            <button
                              className='btn btn-link'
                              data-toggle='collapse'
                              data-target='#collapse10'
                              aria-expanded='true'
                              aria-controls='collapse10'>
                              How delete my account?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse10'
                          className='collapse'
                          aria-labelledby='heading20'
                          data-parent='#accordion-4'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading30'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse20'
                              aria-expanded='false'
                              aria-controls='collapse20'>
                              Where is the edit optioon on dashboard?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse20'
                          className='collapse'
                          aria-labelledby='heading30'
                          data-parent='#accordion-4'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                      <div className='card'>
                        <div className='card-header' id='heading40'>
                          <h5>
                            <button
                              className='btn btn-link collapsed'
                              data-toggle='collapse'
                              data-target='#collapse30'
                              aria-expanded='false'
                              aria-controls='collapse30'>
                              Is there any custome pricing system?
                            </button>
                          </h5>
                        </div>
                        <div
                          id='collapse30'
                          className='collapse'
                          aria-labelledby='heading40'
                          data-parent='#accordion-4'
                          style={{}}>
                          <div className='card-body'>
                            <p>
                              Easy peasy owt to do with me cras I don't want no
                              agro what a load of rubbish starkers absolutely
                              bladdered, old tinkety tonk old fruit so I said
                              the full monty.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* /.tabs-wrapper */}
              <div className='btn-container text-center mt-40 wow pixFadeUp'>
                <a href='#' className='pix-btn btn-outline'>
                  Explore Forum
                </a>
              </div>
              {/* /.btn-container text-center */}
            </div>
            {/* /.container */}
          </div>
          {/* /.faq-section */}
          <div className='scroll-circle wow pixFadeLeft'>
            <img src={Circle8} data-parallax='{"y" : 130}' alt='circle' />
          </div>
        </section>
        {/* /.pricing */}
        {/*==================================*/}
        {/*=         Call To Action         =*/}
        {/*==================================*/}
        {/* <section className='call-to-action'>
          <div className='overlay-bg'>
            <img src={Ellipse} alt='bg' />
          </div>
          <div className='container'>
            <div className='action-content text-center wow pixFadeUp'>
              <h2 className='title'>
                We are optimists who
                <br />
                love to work together
              </h2>
              <p>
                Spiffing good time nice one mush ummm I'm telling down the pub
                spend
                <br />a penny only a quid such a fibber tomfoolery, on your
                bike.
              </p>
              <a href='#' className='pix-btn btn-light'>
                Free Consultation
              </a>
            </div>
          
          </div>
     
          <div className='scroll-circle'>
            <img src={Circle13} data-parallax='{"y" : -130}' alt='circle' />
          </div>
        
        </section> */}
      </div>
    </Layout>
  );
};

export default IndexPage;
