/** @format */

import React from 'react';
import Header from './Header';
import '../../dependencies/bootstrap/css/bootstrap.min.css';
import '../../assets/css/app.css';
import '../../chatBoot/css/animate.css';
import '../../chatBoot/css/slick.css';
import '../../chatBoot/css/materialdesignicons.min.css';
import '../../chatBoot/css/line-awesome.min.css';
import '../../chatBoot/css/style.css';
// import '../../chatBoot/css/rtl-style.css';
import '../../chatBoot/css/colors/default-color.css';
import '../../dependencies/components-elegant-icons/css/elegant-icons.min.css';
import '../../dependencies/fontawesome/css/all.min.css';
import '../../dependencies/magnific-popup/css/magnific-popup.css';
import '../../dependencies/simple-line-icons/css/simple-line-icons.css';
import '../../dependencies/swiper/css/swiper.min.css';
import '../../dependencies/wow/css/animate.css';
import Footer from './Footer';

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <>{children}</>
      <Footer />
    </>
  );
};

export default Layout;
