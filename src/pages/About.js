/** @format */

import React from 'react';
import Layout from './Component/Layout';
import Sing from '../media/about/sing.png';
import About1 from '../media/about/1.jpg';
import Feature1 from '../media/feature/20.png';
import Feature2 from '../media/feature/19.png';
import Feature3 from '../media/feature/18.png';
import T1 from '../media/testimonial/1.jpg';
import TQuote from '../media/testimonial/quote.png';
import Circle11 from '../media/background/circle11.png';
import Team1 from '../media/team/1.jpg';
import Team2 from '../media/team/2.jpg';
import Team3 from '../media/team/3.jpg';
import Brand1 from '../media/brand/1.png';
import Brand2 from '../media/brand/2.png';
import Brand3 from '../media/brand/3.png';
import Brand4 from '../media/brand/4.png';
import Brand5 from '../media/brand/5.png';
import Ellipse from '../media/background/ellipse.png';
import Circle13 from '../media/background/circle13.png';

const About = () => {
  return (
    <Layout>
      <section className='page-banner banner banner-one'>
        <div className='container'>
          <div className='page-title-wrapper'>
            <h1 className='page-title'>About Us</h1>
            <ul className='bradcurmed'>
              <li>
                <a href='index.html' rel='noopener noreferrer'>
                  Home
                </a>
              </li>
              <li>About Us</li>
            </ul>
          </div>
          {/* /.page-title-wrapper */}
        </div>
        {/* /.container */}
        <svg
          className='circle'
          data-parallax='{"x" : -200}'
          xmlns='http://www.w3.org/2000/svg'
          xmlnsXlink='http://www.w3.org/1999/xlink'
          width='950px'
          height='950px'>
          <path
            fillRule='evenodd'
            stroke='rgb(250, 112, 112)'
            strokeWidth='100px'
            strokeLinecap='butt'
            strokeLinejoin='miter'
            opacity='0.051'
            fill='none'
            d='M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z'
          />
        </svg>
        <ul className='animate-ball'>
          <li className='ball' />
          <li className='ball' />
          <li className='ball' />
          <li className='ball' />
          <li className='ball' />
        </ul>
      </section>

      <div>
        <section className='about'>
          <div className='container'>
            <div className='row'>
              <div className='col-lg-7'>
                <div className='about-content'>
                  <div className='section-title'>
                    <h3 className='sub-title wow pixFadeUp'>Our History</h3>
                    <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                      A wide range of business
                      <br />
                      and Saas services
                    </h2>
                  </div>
                  {/* /.section-title */}
                  <p
                    className='description wow pixFadeUp'
                    data-wow-delay='0.4s'>
                    Why I say old chap that is spiffing, bite your arm off geeza
                    blag pukka bleeder A bit of how's your father bugger all
                    mate gutted mate, cheesed off hunky-dory gosh morish get
                    stuffed mate lemon squeezy. Bodge barmy bugger no biggie
                    bleeding bits and bobs my good sir, spend a penny faff about
                    are you taking the piss what a plonker bobby bevvy, chancer
                    Eaton me old mucker don't get shirty with me is.
                  </p>
                  <div
                    className='singiture wow pixFadeUp'
                    data-wow-delay='0.5s'>
                    <h4>Max Conversion</h4>
                    <img
                      src={Sing}
                      className='wow pixFadeUp'
                      data-wow-delay='0.6s'
                      alt='sign'
                    />
                  </div>
                  {/* /.singiture */}
                </div>
                {/* /.about-content */}
              </div>
              {/* /.col-lg-7 */}
              <div className='col-lg-5'>
                <div
                  className='about-thumb wow pixFadeRight'
                  data-wow-delay='0.6s'>
                  <img src={About1} alt='about' />
                </div>
                {/* /.about-thumb */}
              </div>
              {/* /.col-lg-5 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </section>
        {/* /.about */}
        {/*===========================*/}
        {/*=         Feature         =*/}
        {/*===========================*/}
        <section className='featured-four-ab'>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Our service</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                Why you choose Our plugin
              </h2>
            </div>
            {/* /.section-title */}
            <div className='row justify-content-center'>
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-four wow pixFadeLeft'
                  data-wow-delay='0.9s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Feature1} alt />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>New sharing made for people and world</a>
                    </h3>
                    <p>
                      The full monty twit cracking goal Jeffrey lurgy chinwag
                      bobby vagabond David, I don't want no agro what a load.!
                    </p>
                    <a href='#' className='more-btn'>
                      <i className='ei ei-arrow_right' />
                    </a>
                  </div>
                  <svg
                    className='layer'
                    xmlns='http://www.w3.org/2000/svg'
                    xmlnsXlink='http://www.w3.org/1999/xlink'
                    width='370px'
                    height='270px'>
                    <path
                      fillRule='evenodd'
                      fill='rgb(253, 248, 248)'
                      d='M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z'
                    />
                  </svg>
                </div>
                {/* /.pixsass-box style-four */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-four wow pixFadeLeft'
                  data-wow-delay='0.6s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Feature2} alt />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>One integrated solution Manage</a>
                    </h3>
                    <p>
                      The full monty twit cracking goal Jeffrey lurgy chinwag
                      bobby vagabond David, I don't want no agro what a load.!
                    </p>
                    <a href='#' className='more-btn'>
                      <i className='ei ei-arrow_right' />
                    </a>
                  </div>
                  <svg
                    className='layer'
                    xmlns='http://www.w3.org/2000/svg'
                    xmlnsXlink='http://www.w3.org/1999/xlink'
                    width='370px'
                    height='270px'>
                    <path
                      fillRule='evenodd'
                      fill='rgb(253, 248, 248)'
                      d='M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z'
                    />
                  </svg>
                </div>
                {/* /.pixsass-box style-four */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-lg-4 col-md-6'>
                <div
                  className='saaspik-icon-box-wrapper style-four wow pixFadeLeft'
                  data-wow-delay='0.9s'>
                  <div className='saaspik-icon-box-icon'>
                    <img src={Feature3} alt />
                  </div>
                  <div className='pixsass-icon-box-content'>
                    <h3 className='pixsass-icon-box-title'>
                      <a href='#'>Thousand of features and Custom option.</a>
                    </h3>
                    <p>
                      The full monty twit cracking goal Jeffrey lurgy chinwag
                      bobby vagabond David, I don't want no agro what a load.!
                    </p>
                    <a href='#' className='more-btn'>
                      <i className='ei ei-arrow_right' />
                    </a>
                  </div>
                  <svg
                    className='layer'
                    xmlns='http://www.w3.org/2000/svg'
                    xmlnsXlink='http://www.w3.org/1999/xlink'
                    width='370px'
                    height='270px'>
                    <path
                      fillRule='evenodd'
                      fill='rgb(253, 248, 248)'
                      d='M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z'
                    />
                  </svg>
                </div>
                {/* /.pixsass-box style-four */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </section>
        {/* /.featured */}
        {/*===========================*/}
        {/*=         Count Up        =*/}
        {/*===========================*/}
        <section className='countup'>
          <div
            className='bg-map'
            data-bg-image='media/background/map2.png'></div>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Fun Facts</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.2s'>
                We Always try to Understand
                <br />
                Users expectation
              </h2>
            </div>
            {/* /.section-title */}
            <div className='countup-wrapper'>
              <div className='row'>
                <div className='col-lg-3 col-md-6 col-sm-6'>
                  <div className='fun-fact wow pixFadeUp' data-wow-delay='0.3s'>
                    <div className='counter'>
                      <h4 className='count' data-counter={14}>
                        14{' '}
                      </h4>
                      <span>K+</span>
                    </div>
                    {/* /.counter */}
                    <p>Total Download</p>
                  </div>
                  {/* /.fun-fact */}
                </div>
                {/* /.col-lg-3 col-md-6 col-sm-6 */}
                <div className='col-lg-3 col-md-6 col-sm-6'>
                  <div
                    className='fun-fact color-two wow pixFadeUp'
                    data-wow-delay='0.5s'>
                    <div className='counter'>
                      <h4 className='count' data-counter={13}>
                        13{' '}
                      </h4>
                      <span>M+</span>
                    </div>
                    {/* /.counter */}
                    <p>Total Download</p>
                  </div>
                  {/* /.fun-fact */}
                </div>
                {/* /.col-lg-3 col-md-6 col-sm-6 */}
                <div className='col-lg-3 col-md-6 col-sm-6'>
                  <div
                    className='fun-fact color-three wow pixFadeUp'
                    data-wow-delay='0.7s'>
                    <div className='counter'>
                      <h4 className='count' data-counter={244}>
                        244{' '}
                      </h4>
                      <span>+</span>
                    </div>
                    {/* /.counter */}
                    <p>Total Download</p>
                  </div>
                  {/* /.fun-fact */}
                </div>
                {/* /.col-lg-3 col-md-6 col-sm-6 */}
                <div className='col-lg-3 col-md-6 col-sm-6'>
                  <div
                    className='fun-fact color-four wow pixFadeUp'
                    data-wow-delay='0.9s'>
                    <div className='counter'>
                      <h4 className='count' data-counter={53}>
                        53{' '}
                      </h4>
                      <span>M+</span>
                    </div>
                    {/* /.counter */}
                    <p>Total Download</p>
                  </div>
                  {/* /.fun-fact */}
                </div>
                {/* /.col-lg-3 col-md-6 col-sm-6 */}
              </div>
              {/* /.row */}
            </div>
            {/* /.countup-wrapper */}
            <div
              className='button-container text-center wow pixFadeUp'
              data-wow-delay='0.3s'>
              <a href='#' className='pix-btn btn-outline'>
                See All Review
              </a>
            </div>
            {/* /.button-container */}
          </div>
          {/* /.container */}
        </section>
        {/* /.countup */}
        {/*===============================*/}
        {/*=         Testimonial         =*/}
        {/*===============================*/}
        <section className='testimonials-two-about'>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Testiimonial</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                What our client say about us
              </h2>
            </div>
            {/* /.section-title */}
            <div id='testimonial-wrapper'>
              <div
                className='swiper-container wow pixFadeUp'
                data-wow-delay='0.5s'
                id='testimonial-two'
                data-speed={700}
                data-autoplay={5000}
                data-perpage={2}
                data-space={50}
                data-breakpoints='{"991": {"slidesPerView": 1}}'>
                <div className='swiper-wrapper'>
                  <div className='swiper-slide'>
                    <div className='testimonial-two'>
                      <div className='testi-content-inner'>
                        <div className='testimonial-bio'>
                          <div className='avatar'>
                            <img src={T1} alt='testimonial' />
                          </div>
                          {/* /.avatar */}
                          <div className='bio-info'>
                            <h3 className='name'>Desmond Eagle</h3>
                            <span className='job'>Web designer</span>
                          </div>
                        </div>
                        {/* /.testimonial */}
                        <div className='testimonial-content'>
                          <p>
                            Tosser nancy boy super tickety-boo lemon squeezy
                            easy peasy quaint, hunky-dory baking cakes pear
                            shaped butty do one, it's all gone to pot chinwag.!
                          </p>
                        </div>
                        {/* /.testimonial-content */}
                        <ul className='rating'>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                        </ul>
                        <div className='quote'>
                          <img src={TQuote} alt='quote' />
                        </div>
                        {/* /.quote */}
                      </div>
                      {/* /.testi-content-inner */}
                      <div className='shape-shadow' />
                    </div>
                    {/* /.testimonial-two */}
                  </div>
                  <div className='swiper-slide'>
                    <div className='testimonial-two'>
                      <div className='testi-content-inner'>
                        <div className='testimonial-bio'>
                          <div className='avatar'>
                            <img src={T1} alt='testimonial' />
                          </div>
                          {/* /.avatar */}
                          <div className='bio-info'>
                            <h3 className='name'>Desmond Eagle</h3>
                            <span className='job'>Web designer</span>
                          </div>
                        </div>
                        {/* /.testimonial */}
                        <div className='testimonial-content'>
                          <p>
                            Tosser nancy boy super tickety-boo lemon squeezy
                            easy peasy quaint, hunky-dory baking cakes pear
                            shaped butty do one, it's all gone to pot chinwag.!
                          </p>
                        </div>
                        {/* /.testimonial-content */}
                        <ul className='rating'>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                        </ul>
                        <div className='quote'>
                          <img src={TQuote} alt='quote' />
                        </div>
                      </div>
                      {/* /.testi-content-inner */}
                      <div className='shape-shadow' />
                    </div>
                    {/* /.testimonial-two */}
                  </div>
                  <div className='swiper-slide'>
                    <div className='testimonial-two'>
                      <div className='testi-content-inner'>
                        <div className='testimonial-bio'>
                          <div className='avatar'>
                            <img src={T1} alt='testimonial' />
                          </div>
                          {/* /.avatar */}
                          <div className='bio-info'>
                            <h3 className='name'>Desmond Eagle</h3>
                            <span className='job'>Web designer</span>
                          </div>
                        </div>
                        {/* /.testimonial */}
                        <div className='testimonial-content'>
                          <p>
                            Tosser nancy boy super tickety-boo lemon squeezy
                            easy peasy quaint, hunky-dory baking cakes pear
                            shaped butty do one, it's all gone to pot chinwag.!
                          </p>
                        </div>
                        {/* /.testimonial-content */}
                        <ul className='rating'>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                          <li>
                            <i className='fa fa-star' />
                          </li>
                        </ul>
                        <div className='quote'>
                          <img src={TQuote} alt='quote' />
                        </div>
                      </div>
                      {/* /.testi-content-inner */}
                      <div className='shape-shadow' />
                    </div>
                    {/* /.testimonial-two */}
                  </div>
                </div>
                {/* /.swiper-wrapper */}
              </div>
              {/* /.swiper-container */}
              <div className='shape-shadow' />
              <div className='slider-nav wow pixFadeUp' data-wow-delay='0.3s'>
                {/* <div id='slide-prev' className='swiper-button-prev'>
                  <i className='ei ei-arrow_left' />
                </div>
                <div id='slide-next' className=' swiper-button-next'>
                  <i className='ei ei-arrow_right' />
                </div> */}
              </div>
            </div>
            {/* /#testimonial-wrapper */}
          </div>
          {/* /.container */}
          <div className='scroll-circle'>
            <img src={Circle11} data-parallax='{"y" : 250}' alt='shape' />
          </div>
        </section>
        {/* /.testimonial */}
        {/*========================*/}
        {/*=         Team         =*/}
        {/*========================*/}
        <section className='teams'>
          <div className='container'>
            <div className='section-title text-center'>
              <h3 className='sub-title wow pixFadeUp'>Meet The Team</h3>
              <h2 className='title wow pixFadeUp' data-wow-delay='0.3s'>
                Awesome pix saas Team
              </h2>
            </div>
            {/* /.section-title text-center */}
            <div className='row'>
              <div className='col-lg-4 col-md-6'>
                <div
                  className='team-member wow pixFadeRight'
                  data-wow-delay='0.3s'>
                  <div className='member-avater'>
                    <img src={Team1} alt='avater' />
                    <ul className='member-social'>
                      <li>
                        <a href='#'>
                          <i className='fab fa-facebook-f' />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i className='fab fa-twitter' />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i className='fab fa-linkedin-in' />
                        </a>
                      </li>
                    </ul>
                    <svg
                      className='layer-one'
                      xmlns='http://www.w3.org/2000/svg'
                      xmlnsXlink='http://www.w3.org/1999/xlink'
                      width='370px'
                      height='210px'>
                      <path
                        fillRule='evenodd'
                        opacity='0.302'
                        fill='rgb(250, 112, 112)'
                        d='M-0.000,79.999 C-0.000,79.999 85.262,-11.383 187.324,50.502 C297.703,117.429 370.000,-0.001 370.000,-0.001 L370.000,203.999 C370.000,207.313 367.314,209.999 364.000,209.999 L6.000,209.999 C2.686,209.999 -0.000,207.313 -0.000,203.999 L-0.000,79.999 Z'
                      />
                    </svg>
                    <svg
                      className='layer-two'
                      xmlns='http://www.w3.org/2000/svg'
                      xmlnsXlink='http://www.w3.org/1999/xlink'
                      width='370px'
                      height='218px'>
                      <path
                        fillRule='evenodd'
                        opacity='0.8'
                        fill='rgb(250, 112, 112)'
                        d='M-0.000,27.999 C-0.000,27.999 95.694,-46.224 188.615,48.781 C278.036,140.208 370.000,57.999 370.000,57.999 L370.000,211.999 C370.000,215.313 367.314,217.999 364.000,217.999 L6.000,217.999 C2.686,217.999 -0.000,215.313 -0.000,211.999 L-0.000,27.999 Z'
                      />
                    </svg>
                  </div>
                  <div className='team-info'>
                    <h3 className='name'>Dylan Meringue</h3>
                    <h4 className='job'>Project Manager</h4>
                  </div>
                </div>
                {/* /.team-member */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-lg-4 col-md-6'>
                <div
                  className='team-member wow pixFadeRight'
                  data-wow-delay='0.5s'>
                  <div className='member-avater'>
                    <img src={Team2} alt='avater' />
                    <ul className='member-social'>
                      <li>
                        <a href='#'>
                          <i className='fab fa-facebook-f' />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i className='fab fa-twitter' />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i className='fab fa-linkedin-in' />
                        </a>
                      </li>
                    </ul>
                    <svg
                      className='layer-one'
                      xmlns='http://www.w3.org/2000/svg'
                      xmlnsXlink='http://www.w3.org/1999/xlink'
                      width='370px'
                      height='210px'>
                      <path
                        fillRule='evenodd'
                        opacity='0.302'
                        fill='rgb(250, 112, 112)'
                        d='M-0.000,79.999 C-0.000,79.999 85.262,-11.383 187.324,50.502 C297.703,117.429 370.000,-0.001 370.000,-0.001 L370.000,203.999 C370.000,207.313 367.314,209.999 364.000,209.999 L6.000,209.999 C2.686,209.999 -0.000,207.313 -0.000,203.999 L-0.000,79.999 Z'
                      />
                    </svg>
                    <svg
                      className='layer-two'
                      xmlns='http://www.w3.org/2000/svg'
                      xmlnsXlink='http://www.w3.org/1999/xlink'
                      width='370px'
                      height='218px'>
                      <path
                        fillRule='evenodd'
                        opacity='0.8'
                        fill='rgb(250, 112, 112)'
                        d='M-0.000,27.999 C-0.000,27.999 95.694,-46.224 188.615,48.781 C278.036,140.208 370.000,57.999 370.000,57.999 L370.000,211.999 C370.000,215.313 367.314,217.999 364.000,217.999 L6.000,217.999 C2.686,217.999 -0.000,215.313 -0.000,211.999 L-0.000,27.999 Z'
                      />
                    </svg>
                  </div>
                  <div className='team-info'>
                    <h3 className='name'>Lance Bogrol</h3>
                    <h4 className='job'>Developer</h4>
                  </div>
                </div>
                {/* /.team-member */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
              <div className='col-lg-4 col-md-6'>
                <div
                  className='team-member wow pixFadeRight'
                  data-wow-delay='0.7s'>
                  <div className='member-avater'>
                    <img src={Team3} alt='avater' />
                    <ul className='member-social'>
                      <li>
                        <a href='#'>
                          <i className='fab fa-facebook-f' />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i className='fab fa-twitter' />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i className='fab fa-linkedin-in' />
                        </a>
                      </li>
                    </ul>
                    <svg
                      className='layer-one'
                      xmlns='http://www.w3.org/2000/svg'
                      xmlnsXlink='http://www.w3.org/1999/xlink'
                      width='370px'
                      height='210px'>
                      <path
                        fillRule='evenodd'
                        opacity='0.302'
                        fill='rgb(250, 112, 112)'
                        d='M-0.000,79.999 C-0.000,79.999 85.262,-11.383 187.324,50.502 C297.703,117.429 370.000,-0.001 370.000,-0.001 L370.000,203.999 C370.000,207.313 367.314,209.999 364.000,209.999 L6.000,209.999 C2.686,209.999 -0.000,207.313 -0.000,203.999 L-0.000,79.999 Z'
                      />
                    </svg>
                    <svg
                      className='layer-two'
                      xmlns='http://www.w3.org/2000/svg'
                      xmlnsXlink='http://www.w3.org/1999/xlink'
                      width='370px'
                      height='218px'>
                      <path
                        fillRule='evenodd'
                        opacity='0.8'
                        fill='rgb(250, 112, 112)'
                        d='M-0.000,27.999 C-0.000,27.999 95.694,-46.224 188.615,48.781 C278.036,140.208 370.000,57.999 370.000,57.999 L370.000,211.999 C370.000,215.313 367.314,217.999 364.000,217.999 L6.000,217.999 C2.686,217.999 -0.000,215.313 -0.000,211.999 L-0.000,27.999 Z'
                      />
                    </svg>
                  </div>
                  <div className='team-info'>
                    <h3 className='name'>Dylan Meringue</h3>
                    <h4 className='job'>UI/UX designer</h4>
                  </div>
                </div>
                {/* /.team-member */}
              </div>
              {/* /.col-lg-4 col-md-6 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </section>
        {/* /.teams */}
        {/*=================================*/}
        {/*=         Logo Carousel         =*/}
        {/*=================================*/}
        <section className='brand-logo-ab'>
          <div className='section-small text-center'>
            <h2 className='title wow pixFadeUp'>
              Trusted Over 2500+ Companies
            </h2>
          </div>
          <div className='container'>
            <div
              className='swiper-container logo-carousel wow pixFadeUp'
              id='logo-carousel'
              data-perpage={6}
              data-breakpoints='{"1024": {"slidesPerView": 4}, "768": {"slidesPerView": 4}, "620": {"slidesPerView": 3}}'>
              <div className='swiper-wrapper'>
                <div className='swiper-slide'>
                  <div className='brand-logo'>
                    <img src={Brand1} alt='brand' />
                  </div>
                </div>
                {/* /.swiper-slide */}
                <div className='swiper-slide'>
                  <div className='brand-logo'>
                    <img src={Brand1} alt='brand' />
                  </div>
                </div>
                {/* /.swiper-slide */}
                <div className='swiper-slide'>
                  <div className='brand-logo'>
                    <img src={Brand2} alt='brand' />
                  </div>
                </div>
                {/* /.swiper-slide */}
                <div className='swiper-slide'>
                  <div className='brand-logo'>
                    <img src={Brand3} alt='brand' />
                  </div>
                </div>
                {/* /.swiper-slide */}
                <div className='swiper-slide'>
                  <div className='brand-logo'>
                    <img src={Brand4} alt='brand' />
                  </div>
                </div>
                {/* /.swiper-slide */}
                <div className='swiper-slide'>
                  <div className='brand-logo'>
                    <img src={Brand5} alt='brand' />
                  </div>
                </div>
                {/* /.swiper-slide */}
              </div>
              {/* /.swiper-wrapper */}
            </div>
            {/* /#logo-carousel.swiper-container logo-carousel */}
          </div>
          {/* /.container */}
        </section>
        {/* /.brand-logo */}
        {/*==================================*/}
        {/*=         Call To Action         =*/}
        {/*==================================*/}
        <section className='call-to-action'>
          <div className='overlay-bg'>
            <img src={Ellipse} alt='bg' />
          </div>
          <div className='container'>
            <div className='action-content text-center wow pixFadeUp'>
              <h2 className='title'>
                We are optimists who
                <br />
                love to work together
              </h2>
              <p>
                Spiffing good time nice one mush ummm I'm telling down the pub
                spend
                <br />a penny only a quid such a fibber tomfoolery, on your
                bike.
              </p>
              <a href='#' className='pix-btn btn-light'>
                Free Consultation
              </a>
            </div>
            {/* /.action-content */}
          </div>
          {/* /.container */}
          <div className='scroll-circle'>
            <img src={Circle13} data-parallax='{"y" : -130}' alt='circle' />
          </div>
          {/* /.scroll-circle */}
        </section>
      </div>
    </Layout>
  );
};

export default About;
