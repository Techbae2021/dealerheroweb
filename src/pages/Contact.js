/** @format */

import React from 'react';
import Layout from './Component/Layout';
import A1 from '../media/animated/001.png';
import A2 from '../media/animated/002.png';
import A3 from '../media/animated/003.png';
import A4 from '../media/animated/004.png';
import Maps from '../assets/img/map-marker.png';

const Contact = () => {
  return (
    <Layout>
      <div>
        <section className='page-banner-contact banner banner-one'>
          <div className='container'>
            <div className='row'>
              <div className='col-lg-8'>
                <div className='page-title-wrapper'>
                  <div className='page-title-inner'>
                    <h1 className='page-title'>Get in touch with Us</h1>
                    <p>
                      Why I say old chap that is spiffing, young delinquent in
                      my flat bloke
                      <br />
                      buggered what a plonker.
                    </p>
                  </div>
                  {/* /.page-title-inner */}
                </div>
                {/* /.page-title-wrapper */}
              </div>
              {/* /.col-lg-8 */}
              <div className='col-lg-4'>
                <div className='animate-element-contact'>
                  <img
                    src={A1}
                    alt
                    className='wow pixFadeDown'
                    data-wow-duration='1s'
                  />
                  <img
                    src={A2}
                    alt
                    className='wow pixFadeUp'
                    data-wow-duration='2s'
                  />
                  <img
                    src={A3}
                    alt
                    className='wow pixFadeLeft'
                    data-wow-delay='0.3s'
                    data-wow-duration='2s'
                  />
                  <img
                    src={A4}
                    alt='man'
                    className='wow pixFadeUp'
                    data-wow-duration='2s'
                  />
                </div>
                {/* /.animate-element-contact */}
              </div>
              {/* /.col-lg-4 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
          <svg
            className='circle'
            data-parallax='{"y" : 250}'
            xmlns='http://www.w3.org/2000/svg'
            xmlnsXlink='http://www.w3.org/1999/xlink'
            width='950px'
            height='950px'>
            <path
              fillRule='evenodd'
              stroke='rgb(250, 112, 112)'
              strokeWidth='100px'
              strokeLinecap='butt'
              strokeLinejoin='miter'
              opacity='0.051'
              fill='none'
              d='M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z'
            />
          </svg>
          <ul className='animate-ball'>
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
          </ul>
        </section>
        {/* /.page-banner */}
        {/*===========================*/}
        {/*=         Contact         =*/}
        {/*===========================*/}
        <section className='contactus'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-4'>
                <div className='contact-infos'>
                  <div className='contact-info'>
                    <h3 className='title'>Our Location</h3>
                    <p className='description'>
                      46 Eucalyptus Drive, Maidstone,
                      <br />
                      Melbourne, Victoria 3012
                    </p>
                    <div className='info phone'>
                      <i className='ei ei-icon_phone' />
                      <span>0427497042</span>
                    </div>
                  </div>
                  {/* /.contact-info */}
                  <div className='contact-info'>
                    <h3 className='title'>Say Hello</h3>
                    <p className='description'>
                      Washington Fulton Street, Suite 640
                      <br />
                      New York, NY 10010
                    </p>
                    <div className='info'>
                      <i className='ei ei-icon_mail_alt' />
                      <span>info@techbae.com.au</span>
                    </div>
                  </div>
                  {/* /.contact-info */}
                </div>
                {/* /.contact-infos */}
              </div>
              {/* /.col-md-4 */}
              <div className='col-md-8'>
                <div className='contact-froms'>
                  <form
                    action='#'
                    className='contact-form'
                    data-pixsaas='contact-froms'>
                    <div className='row'>
                      <div className='col-md-6'>
                        <input
                          type='text'
                          name='name'
                          placeholder='Name'
                          required
                        />
                      </div>
                      <div className='col-md-6'>
                        <input
                          type='email'
                          name='email'
                          placeholder='Email'
                          required
                        />
                      </div>
                    </div>
                    <input type='text' name='subject' placeholder='Subject' />
                    <textarea
                      name='content'
                      placeholder='Your Comment'
                      required
                      defaultValue={''}
                    />
                    <button type='submit' className='pix-btn submit-btn'>
                      <span className='btn-text'>Send Your Massage</span>
                      <i className='fas fa-spinner fa-spin' />
                    </button>
                    <input
                      type='hidden'
                      name='recaptcha_response'
                      id='recaptchaResponse'
                    />
                    <div className='row'>
                      <div className='form-result alert'>
                        <div className='content' />
                      </div>
                    </div>
                  </form>
                  {/* /.contact-froms */}
                </div>
                {/* /.faq-froms */}
              </div>
              {/* /.col-md-8 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </section>
        {/* /.contactus */}
        {/*========================*/}
        {/*=         Map         =*/}
        {/*========================*/}
        <section id='google-maps'>
          <div className='google-map'>
            <div
              className='gmap3-area'
              data-lat='40.712776'
              data-lng='-74.005974'
              data-mrkr={Maps}></div>
          </div>
          {/* /.google-map */}
        </section>
      </div>
    </Layout>
  );
};

export default Contact;
