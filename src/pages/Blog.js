/** @format */

import React from 'react';
import Layout from './Component/Layout';
import Blog4 from '../media/blog/4.jpg';
import Auth2 from '../media/blog/auth2.jpg';
import Blog5 from '../media/blog/5.jpg';
import BlogQuote from '../media/blog/quote.png';
import Blog12 from '../media/blog/12.jpg';
import Blog11 from '../media/blog/11.jpg';
import Blog8 from '../media/blog/8.jpg';
import Blog9 from '../media/blog/9.jpg';
import Blog10 from '../media/blog/10.jpg';

const Blog = () => {
  return (
    <Layout>
      <div>
        <section className='page-banner banner banner-one'>
          <div className='container'>
            <div className='page-title-wrapper'>
              <h1 className='page-title'>Blog Standard</h1>
              <ul className='bradcurmed'>
                <li>
                  <a href='#' rel='noopener noreferrer'>
                    Home
                  </a>
                </li>
                <li>Blog Standard</li>
              </ul>
            </div>
            {/* /.page-title-wrapper */}
          </div>
          {/* /.container */}
          <svg
            className='circle'
            data-parallax='{"x" : -200}'
            xmlns='http://www.w3.org/2000/svg'
            xmlnsXlink='http://www.w3.org/1999/xlink'
            width='950px'
            height='950px'>
            <path
              fillRule='evenodd'
              stroke='rgb(250, 112, 112)'
              strokeWidth='100px'
              strokeLinecap='butt'
              strokeLinejoin='miter'
              opacity='0.051'
              fill='none'
              d='M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z'
            />
          </svg>
          <ul className='animate-ball'>
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
          </ul>
        </section>
        {/* /.page-banner */}
        {/*========================*/}
        {/*=         Blog         =*/}
        {/*========================*/}
        <div className='blog-post-archive'>
          <div className='container'>
            <div className='row'>
              <div className='col-lg-8'>
                <div className='post-wrapper'>
                  <article className='post'>
                    <div className='feature-image'>
                      <a href='blog-signle.html'>
                        <img src={Blog4} alt />
                      </a>
                    </div>
                    <div className='blog-content'>
                      <ul className='post-meta'>
                        <li>
                          <a href='#'>Oct 16, 2019</a>
                        </li>
                        <li>
                          <a href='#'>4 Comments</a>
                        </li>
                        <li>
                          <a href='#'>Sass,</a>
                          <a href='#'>App</a>
                        </li>
                      </ul>
                      <h3 className='entry-title'>
                        <a href='#'>Young Guns Reflections: Paul Felton</a>
                      </h3>
                      <p>
                        The little rotter spiffing good time lemon squeezy
                        smashing excuse my French old, cheesed off give us a
                        bell happy days brown bread, blow off Harry barney
                        bobby. Cup of char gormless hors.!
                      </p>
                      <a href='/BlogView' className='read-more'>
                        Read More <i className='ei ei-arrow_right' />
                      </a>
                      <div className='author'>
                        <img src={Auth2} alt='author' />
                        <a href='#' className='author-link'>
                          Hans Down
                        </a>
                      </div>
                    </div>
                    {/* /.post-content */}
                  </article>
                  {/* /.post */}
                  <article className='post video-post'>
                    <div className='feature-image'>
                      <a href='blog-signle.html'>
                        <img src={Blog5} alt />
                      </a>
                      <a
                        href='https://www.youtube.com/watch?v=9No-FiEInLA'
                        className='video-btn popup-video'>
                        <i className='ei ei-arrow_triangle-right' />
                      </a>
                    </div>
                    <div className='blog-content'>
                      <ul className='post-meta'>
                        <li>
                          <a href='#'>Oct 16, 2019</a>
                        </li>
                        <li>
                          <a href='#'>4 Comments</a>
                        </li>
                        <li>
                          <a href='#'>Sass,</a>
                          <a href='#'>App</a>
                        </li>
                      </ul>
                      <h3 className='entry-title'>
                        <a href='#'>The Belgian Exodus of World war one</a>
                      </h3>
                      <p>
                        The little rotter spiffing good time lemon squeezy
                        smashing excuse my French old, cheesed off give us a
                        bell happy days brown bread, blow off Harry barney
                        bobby. Cup of char gormless hors.!
                      </p>
                      <a href='/BlogView' className='read-more'>
                        Read More <i className='ei ei-arrow_right' />
                      </a>
                      <div className='author'>
                        <img src={Auth2} alt='author' />
                        <a href='#' className='author-link'>
                          Hans Down
                        </a>
                      </div>
                    </div>
                    {/* /.post-content */}
                  </article>
                  {/* /.post */}
                  <article className='post quote-post'>
                    <p>
                      Horse play a blinding shot the little rotter nice one umm
                      I'm telling bits and bobs grub boot that bevvy, cockup
                      matie boy mush Jeffrey.!
                    </p>
                    <span className='quote-author'>Druid Wensleydale</span>
                    <span className='quote'>
                      <img src={BlogQuote} alt />
                    </span>
                  </article>
                  {/* /.post */}
                  <article className='post'>
                    <div className='feature-image'>
                      <a href='blog-signle.html'>
                        <img src={Blog12} alt />
                      </a>
                    </div>
                    <div className='blog-content'>
                      <ul className='post-meta'>
                        <li>
                          <a href='#'>Oct 16, 2019</a>
                        </li>
                        <li>
                          <a href='#'>4 Comments</a>
                        </li>
                        <li>
                          <a href='#'>Sass,</a>
                          <a href='#'>App</a>
                        </li>
                      </ul>
                      <h3 className='entry-title'>
                        <a href='#'>Join Us For a Delicious Event</a>
                      </h3>
                      <p>
                        The little rotter spiffing good time lemon squeezy
                        smashing excuse my French old, cheesed off give us a
                        bell happy days brown bread, blow off Harry barney
                        bobby. Cup of char gormless hors.!
                      </p>
                      <a href='/BlogView' className='read-more'>
                        Read More <i className='ei ei-arrow_right' />
                      </a>
                      <div className='author'>
                        <img src={Auth2} alt='author' />
                        <a href='#' className='author-link'>
                          Hans Down
                        </a>
                      </div>
                    </div>
                    {/* /.post-content */}
                  </article>
                  {/* /.post */}
                  <article className='post link-post'>
                    <div className='blog-content'>
                      <p>
                        <a href='#'>
                          The little rotter spiffing good time lemon squeezy
                          smashing excuse my french old.!
                        </a>
                      </p>
                    </div>
                    {/* /.post-content */}
                  </article>
                  {/* /.post */}
                  <article className='post'>
                    <div className='feature-image'>
                      <a href='blog-signle.html'>
                        <img src={Blog11} alt />
                      </a>
                    </div>
                    <div className='blog-content'>
                      <ul className='post-meta'>
                        <li>
                          <a href='#'>Oct 16, 2019</a>
                        </li>
                        <li>
                          <a href='#'>4 Comments</a>
                        </li>
                        <li>
                          <a href='#'>Sass,</a>
                          <a href='#'>App</a>
                        </li>
                      </ul>
                      <h3 className='entry-title'>
                        <a href='#'>The Belgian Exodus of World war one</a>
                      </h3>
                      <p>
                        The little rotter spiffing good time lemon squeezy
                        smashing excuse my French old, cheesed off give us a
                        bell happy days brown bread, blow off Harry barney
                        bobby. Cup of char gormless hors.!
                      </p>
                      <a href='/BlogView' className='read-more'>
                        Read More <i className='ei ei-arrow_right' />
                      </a>
                      <div className='author'>
                        <img src={Auth2} alt='author' />
                        <a href='#' className='author-link'>
                          Hans Down
                        </a>
                      </div>
                    </div>
                    {/* /.post-content */}
                  </article>
                  {/* /.post */}
                  <ul className='post-navigation'>
                    <li className='active'>1</li>
                    <li>
                      <a href='#'>2</a>
                    </li>
                    <li className='next'>
                      <a href='#'>
                        <i className='ei ei-arrow_carrot-right' />
                      </a>
                    </li>
                  </ul>
                </div>
                {/* /.post-wrapper */}
              </div>
              {/* /.col-lg-8 */}
              <div className='col-lg-4'>
                <div className='sidebar'>
                  <div id='search' className='widget widget_search'>
                    <form role='search' className='search-form-widget'>
                      <label>
                        <input
                          type='search'
                          className='search-field'
                          placeholder='Search...'
                        />
                      </label>
                      <button type='submit' className='search-submit'>
                        <i className='ei ei-icon_search' />
                      </button>
                    </form>
                  </div>
                  <div id='categories' className='widget widget_categories'>
                    <h2 className='widget-title'>Categories</h2>
                    <ul>
                      <li>
                        <a href='#'>
                          Web Design <span className='count'>(6)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          Web Development <span className='count'>(14)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          Graphics <span className='count'>(12)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          IOS/Android Design <span className='count'>(10)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          App &amp; Saas <span className='count'>(4)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          Fresh Products <span className='count'>(9)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          Saas Design <span className='count'>(8)</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div
                    id='gp-posts-widget-2'
                    className='widget gp-posts-widget'>
                    <h2 className='widget-title'>Latest News</h2>
                    <div className='gp-posts-widget-wrapper'>
                      <div className='post-item'>
                        <div className='post-widget-thumbnail'>
                          <a href='#'>
                            <img src={Blog8} alt='thumb' />
                          </a>
                        </div>
                        <div className='post-widget-info'>
                          <h5 className='post-widget-title'>
                            <a href='#' title='This Is Test Post'>
                              The Festive Season is Approaching
                            </a>
                          </h5>
                          <span className='post-date'>
                            May 16, 2019, Saturday
                          </span>
                        </div>
                      </div>
                      <div className='post-item'>
                        <div className='post-widget-thumbnail'>
                          <a href='#'>
                            <img src={Blog9} alt='thumb' />
                          </a>
                        </div>
                        <div className='post-widget-info'>
                          <h5 className='post-widget-title'>
                            <a href='#' title='This Is Test Post'>
                              Shrimp Scampi With Linguini
                            </a>
                          </h5>
                          <span className='post-date'>
                            Apri 16, 2019, Thursday
                          </span>
                        </div>
                      </div>
                      <div className='post-item'>
                        <div className='post-widget-thumbnail'>
                          <a href='#'>
                            <img src={Blog10} alt='thumb' />
                          </a>
                        </div>
                        <div className='post-widget-info'>
                          <h5 className='post-widget-title'>
                            <a href='#' title='This Is Test Post'>
                              Join Us For a Delicious Event
                            </a>
                          </h5>
                          <span className='post-date'>
                            Oct 16, 2019, Monday
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    id='widget_recent_comments'
                    className='widget widget_recent_comments'>
                    <h2 className='widget-title'>Recent Comments</h2>
                    <div className='recent-comments'>
                      <div className='comment-list'>
                        <div className='icon'>
                          <i className='ei ei-icon_chat_alt' />
                        </div>
                        <div className='comment-content'>
                          <h3>
                            Bailey Wonger <span>on</span>
                          </h3>
                          <p>
                            <a href='#'>
                              My lady mush hanky panky young delinquent.!
                            </a>
                          </p>
                        </div>
                      </div>
                      {/* /.comment-list */}
                      <div className='comment-list'>
                        <div className='icon'>
                          <i className='ei ei-icon_chat_alt' />
                        </div>
                        <div className='comment-content'>
                          <h3>
                            Hans Down <span>on</span>
                          </h3>
                          <p>
                            <a href='#'>
                              Why I say old chap that is <br />
                              spiffing daft, blow.!
                            </a>
                          </p>
                        </div>
                      </div>
                      {/* /.comment-list */}
                      <div className='comment-list'>
                        <div className='icon'>
                          <i className='ei ei-icon_chat_alt' />
                        </div>
                        <div className='comment-content'>
                          <h3>
                            Justin Case <span>on</span>
                          </h3>
                          <p>
                            <a href='#'>
                              Tomfoolery hanky panky
                              <br /> geeza I my good.!
                            </a>
                          </p>
                        </div>
                      </div>
                      {/* /.comment-list */}
                    </div>
                    {/* /.recent-comments */}
                  </div>
                  <aside id='tags' className='widget widget_tag'>
                    <h3 className='widget-title'>Popular Tags</h3>
                    <div className='tagcloud'>
                      <a href='#'>App &amp; Saas</a>
                      <a href='#'>UI/ Design</a>
                      <a href='#'>Branding</a>
                      <a href='#'>Design</a>
                      <a href='#'>Landing</a>
                      <a href='#'>Pix Saas Blog</a>
                      <a href='#'>Development</a>
                      <a href='#'>The Saas</a>
                    </div>
                  </aside>
                  {/* /.widget */}
                </div>
                {/* /.sidebar */}
              </div>
              {/* /.col-lg-4 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
        </div>
        {/* /.blog-post-archive */}
      </div>
    </Layout>
  );
};

export default Blog;
