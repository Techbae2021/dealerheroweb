/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/ssr-apis/
 *
 * @format
 */

// You can delete this file if you're not using it

import React from 'react';

export const onRenderBody = ({ setPostBodyComponents }) => {
  setPostBodyComponents([
    <script
      key='../dependencies/jquery/jquery.min.js'
      src='../dependencies/jquery/jquery.min.js'
    />,

    <script
      key='../dependencies/bootstrap/js/bootstrap.min.js'
      src='../dependencies/bootstrap/js/bootstrap.min.js'></script>,

    <script
      key='../dependencies/swiper/js/swiper.min.js'
      src='../dependencies/swiper/js/swiper.min.js'></script>,

    <script
      key='../dependencies/jquery.appear/jquery.appear.js'
      src='../dependencies/jquery.appear/jquery.appear.js'></script>,

    <script
      key='../dependencies/wow/js/wow.min.js'
      src='../dependencies/wow/js/wow.min.js'></script>,

    <script
      key='../dependencies/countUp.js/countUp.min.js'
      src='../dependencies/countUp.js/countUp.min.js'></script>,

    <script
      key='../dependencies/isotope-layout/isotope.pkgd.min.js'
      src='../dependencies/isotope-layout/isotope.pkgd.min.js'></script>,

    <script
      key='../dependencies/imagesloaded/imagesloaded.pkgd.min.js'
      src='../dependencies/imagesloaded/imagesloaded.pkgd.min.js'></script>,

    <script
      key='../dependencies/jquery.parallax-scroll/js/jquery.parallax-scroll.js'
      src='../dependencies/jquery.parallax-scroll/js/jquery.parallax-scroll.js'></script>,
    <script
      key='../dependencies/magnific-popup/js/jquery.magnific-popup.min.js'
      src='../dependencies/magnific-popup/js/jquery.magnific-popup.min.js'></script>,

    <script
      key='../dependencies/gmap3/js/gmap3.min.js'
      src='../dependencies/gmap3/js/gmap3.min.js'></script>,

    <script key='../assets/js/header.js' src='../assets/js/header.js'></script>,
    <script key='../assets/js/app.js' src='../assets/js/app.js'></script>,
    // <script
    //   async
    //   defer
    //   src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDVyblIR8xfihJrBpvbpCdSExi3beasygI&callback=initMap'
    //   type='text/javascript'></script>,
  ]);
};
