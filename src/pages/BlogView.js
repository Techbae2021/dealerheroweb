/** @format */

import React from 'react';
import Layout from './Component/Layout';
import Blog4 from '../media/blog/4.jpg';
import Avatar1 from '../media/blog/avatar1.jpg';
import Avatar2 from '../media/blog/avatar1.jpg';
import BlogQuote from '../media/blog/quote.png';
import Blog12 from '../media/blog/12.jpg';
import Blog8 from '../media/blog/8.jpg';
import Blog9 from '../media/blog/9.jpg';
import Blog10 from '../media/blog/10.jpg';
import BlogAuthor from '../media/blog/author.jpg';

const BlogView = () => {
  return (
    <Layout>
      <div>
        <section className='page-banner blog-details-banner banner banner-one'>
          <div className='container'>
            <div className='page-title-wrapper'>
              <ul className='post-meta color-theme'>
                <li>
                  <a href='#'>December 10, 2019</a>
                </li>
              </ul>
              <h1 className='page-title'>
                This response is important for our ability to learn from
                mistakes.
              </h1>
              <ul className='post-meta'>
                <li>
                  <span>By</span> <a href='#'>Piff Jenkins</a>
                </li>
                <li>
                  <a href='#'>4 Comments</a>
                </li>
                <li>
                  <a href='#'>Sass</a>,<a href='#'>App</a>
                </li>
              </ul>
            </div>
            {/* /.page-title-wrapper */}
          </div>
          {/* /.container */}
          <svg
            className='circle'
            data-parallax='{"x" : -200}'
            xmlns='http://www.w3.org/2000/svg'
            xmlnsXlink='http://www.w3.org/1999/xlink'
            width='950px'
            height='950px'>
            <path
              fillRule='evenodd'
              stroke='rgb(250, 112, 112)'
              strokeWidth='100px'
              strokeLinecap='butt'
              strokeLinejoin='miter'
              opacity='0.051'
              fill='none'
              d='M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z'
            />
          </svg>
          <ul className='animate-ball'>
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
            <li className='ball' />
          </ul>
        </section>
        {/* /.page-banner */}
        {/*========================*/}
        {/*=         Blog         =*/}
        {/*========================*/}
        <section className='blog-single'>
          <div className='container pb-120'>
            <div className='row'>
              <div className='col-md-8'>
                <div className='post-wrapper'>
                  <article className='post post-signle'>
                    <div className='feature-image'>
                      <a href='blog-signle.html'>
                        <img src={Blog4} alt />
                      </a>
                    </div>
                    <div className='blog-content'>
                      <p>
                        So I said knackered do one ummm I'm telling up the kyver
                        arse over tit smashing lurgy lost the plot, owt to do
                        with me plastered easy peasy chap it's your round bobby
                        twit say you mug, only a quid quaint I don't want no
                        agro spend a penny it's all gone to pot what a plonker
                        nice one. Up the duff lurgy gutted mate haggle only a
                        quid have it crikey posh chancer some dodgy chav, cor
                        blimey guvnor bog up the kyver William brilliant
                        lavatory absolutely bladdered no biggie, wind up
                        buggered bleeding morish bum bag butty spiffing good
                        time super. Naff the little rotter down the pub happy
                        days cheeky butty cup of char on your bike mate chinwag
                        I blimey Elizabeth blag bobby cheers ummm I'm telling,
                        bleeding nice one bog old up the duff chip shop ruddy
                        cras tosser bog-standard zonked A bit of how's your
                        father sloshed. Spend a penny cack tosser cheeky
                        tickety-boo only a quid Elizabeth ruddy cup of char,
                        ummm I'm telling it's your round arse bamboozled.!
                      </p>
                      <blockquote className='quote-post'>
                        <p>
                          Horse play a blinding shot the little rotter nice one
                          umm I'm telling bits and bobs grub boot that bevvy,
                          cockup matie boy mush Jeffrey.!
                        </p>
                        <span className='quote-author'>Druid Wensleydale</span>
                        <span className='quote'>
                          <img src={BlogQuote} alt />
                        </span>
                      </blockquote>
                      {/* /.post */}
                      <p>
                        Why I say old chap that is spiffing, young delinquent in
                        my flat bloke buggered what a plonker. Give us a bell
                        arse the BBC cheeky victoria sponge say well excuse my
                        French, cheeky bugger bugger all mate what a load of
                        rubbish he lost his bottle matie boy boot, smashing
                        chinwag squiffy my good sir brown bread happy days. My
                        lady tinkety tonk old fruit some dodgy chav.!
                      </p>
                      <img src={Blog12} alt='blog' />
                      <h3>Creative mood UX</h3>
                      <p>
                        Tomfoolery nice one spiffing chip shop cuppa mush
                        chancer bevvy grub, Eaton ummm I'm telling cockup cor
                        blimey guvnor bugger vagabond William blow off, happy
                        days the wireless cobblers up the kyver such a fibber
                        pardon you absolutely bladdered. The little rotter brown
                        bread squiffy bubble and squeak a load of old tosh in my
                        flat, what a load of rubbish I don't want no agro arse
                        over tit bum bag, dropped a clanger.!
                      </p>
                      <div className='tagcloud'>
                        <span>Tags:</span>
                        <a href='#'>The Sass</a>
                        <a href='#'>App &amp; Sass</a>
                      </div>
                    </div>
                    {/* /.post-content */}
                  </article>
                  {/* /.post */}
                  <div className='blog-share'>
                    <div className='share-title'>
                      <p>Share:</p>
                    </div>
                    <ul className='share-link'>
                      <li>
                        <a href='#'>
                          <i className='fab fa-facebook-f' />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i className='fab fa-twitter' />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i className='fab fa-linkedin-in' />
                        </a>
                      </li>
                    </ul>
                  </div>
                  {/* /.blog-share */}
                  <div className='pixsass_post_author_box clearfix'>
                    <div className='profile_image'>
                      <img alt='author' src={BlogAuthor} />
                    </div>
                    {/* /.profile_image */}
                    <div className='profile_content'>
                      <h4 className='profile_name'>Hans Down</h4>
                      <span className='author-job'>About Author</span>
                      <div className='profile_bio'>
                        <p>
                          So I said knackered do one ummm I'm telling up the
                          kyver arse over tit smashing lurgy.!
                        </p>
                      </div>
                    </div>
                    {/* /.profile_content */}
                  </div>
                </div>
                {/* /.post-wrapper */}
              </div>
              {/* /.col-md-8 */}
              <div className='col-md-4'>
                <div className='sidebar'>
                  <div id='search' className='widget widget_search'>
                    <form role='search' className='search-form-widget'>
                      <label>
                        <input
                          type='search'
                          className='search-field'
                          placeholder='Search...'
                        />
                      </label>
                      <button type='submit' className='search-submit'>
                        <i className='ei ei-icon_search' />
                      </button>
                    </form>
                  </div>
                  <div id='categories' className='widget widget_categories'>
                    <h2 className='widget-title'>Categories</h2>
                    <ul>
                      <li>
                        <a href='#'>
                          Web Design <span className='count'>(6)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          Web Development <span className='count'>(14)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          Graphics <span className='count'>(12)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          IOS/Android Design <span className='count'>(10)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          App &amp; Saas <span className='count'>(4)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          Fresh Products <span className='count'>(9)</span>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          Saas Design <span className='count'>(8)</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div
                    id='gp-posts-widget-2'
                    className='widget gp-posts-widget'>
                    <h2 className='widget-title'>Latest News</h2>
                    <div className='gp-posts-widget-wrapper'>
                      <div className='post-item'>
                        <div className='post-widget-thumbnail'>
                          <a href='#'>
                            <img src={Blog8} alt='thumb' />
                          </a>
                        </div>
                        <div className='post-widget-info'>
                          <h5 className='post-widget-title'>
                            <a href='#' title='This Is Test Post'>
                              The Festive Season is Approaching
                            </a>
                          </h5>
                          <span className='post-date'>
                            May 16, 2019, Saturday
                          </span>
                        </div>
                      </div>
                      <div className='post-item'>
                        <div className='post-widget-thumbnail'>
                          <a href='#'>
                            <img src={Blog9} alt='thumb' />
                          </a>
                        </div>
                        <div className='post-widget-info'>
                          <h5 className='post-widget-title'>
                            <a href='#' title='This Is Test Post'>
                              Shrimp Scampi With Linguini
                            </a>
                          </h5>
                          <span className='post-date'>
                            Apri 16, 2019, Thursday
                          </span>
                        </div>
                      </div>
                      <div className='post-item'>
                        <div className='post-widget-thumbnail'>
                          <a href='#'>
                            <img src={Blog10} alt='thumb' />
                          </a>
                        </div>
                        <div className='post-widget-info'>
                          <h5 className='post-widget-title'>
                            <a href='#' title='This Is Test Post'>
                              Join Us For a Delicious Event
                            </a>
                          </h5>
                          <span className='post-date'>
                            Oct 16, 2019, Monday
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    id='widget_recent_comments'
                    className='widget widget_recent_comments'>
                    <h2 className='widget-title'>Recent Comments</h2>
                    <div className='recent-comments'>
                      <div className='comment-list'>
                        <div className='icon'>
                          <i className='ei ei-icon_chat_alt' />
                        </div>
                        <div className='comment-content'>
                          <h3>
                            Bailey Wonger <span>on</span>
                          </h3>
                          <p>
                            <a href='#'>
                              My lady mush hanky panky young delinquent.!
                            </a>
                          </p>
                        </div>
                      </div>
                      {/* /.comment-list */}
                      <div className='comment-list'>
                        <div className='icon'>
                          <i className='ei ei-icon_chat_alt' />
                        </div>
                        <div className='comment-content'>
                          <h3>
                            Hans Down <span>on</span>
                          </h3>
                          <p>
                            <a href='#'>
                              Why I say old chap that is <br />
                              spiffing daft, blow.!
                            </a>
                          </p>
                        </div>
                      </div>
                      {/* /.comment-list */}
                      <div className='comment-list'>
                        <div className='icon'>
                          <i className='ei ei-icon_chat_alt' />
                        </div>
                        <div className='comment-content'>
                          <h3>
                            Justin Case <span>on</span>
                          </h3>
                          <p>
                            <a href='#'>
                              Tomfoolery hanky panky
                              <br /> geeza I my good.!
                            </a>
                          </p>
                        </div>
                      </div>
                      {/* /.comment-list */}
                    </div>
                    {/* /.recent-comments */}
                  </div>
                  <aside id='tags' className='widget widget_tag'>
                    <h3 className='widget-title'>Popular Tags</h3>
                    <div className='tagcloud'>
                      <a href='#'>App &amp; Saas</a>
                      <a href='#'>UI/ Design</a>
                      <a href='#'>Branding</a>
                      <a href='#'>Design</a>
                      <a href='#'>Landing</a>
                      <a href='#'>Pix Saas Blog</a>
                      <a href='#'>Development</a>
                      <a href='#'>The Saas</a>
                    </div>
                  </aside>
                  {/* /.widget */}
                </div>
                {/* /.sidebar */}
              </div>
              {/* /.col-md-4 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.container */}
          <div className='comment-area section-bg'>
            <div className='container'>
              <div className='post-wrapper'>
                <div id='comments' className='comments-area'>
                  <h3 className='coment-title'>2 Comment</h3>
                  <ul className='comment-list'>
                    <li className='comment'>
                      <article className='comment-body'>
                        <div className='comment-author'>
                          <img alt src={Avatar1} />
                        </div>
                        {/* .comment-author */}
                        <div className='comment-content'>
                          <h4 className='fn'>
                            <a href='#'>Jason Response</a>
                          </h4>
                          <div className='comment-metadata'>
                            <a href='#'>July 3, 2018 at 2:14 pm</a>
                          </div>
                          {/* .comment-metadata */}
                          <p>
                            My lady mush hanky panky young delinquent lurgy the
                            little rotter in my flat tomfoolery so I said mufty
                            cockup.!
                          </p>
                          <a
                            rel='nofollow'
                            className='comment-reply-link'
                            href='#'>
                            <i className='ei ei-arrow_back' />
                            Reply
                          </a>
                        </div>
                        {/* .comment-content */}
                      </article>
                      {/* .comment-body */}
                      <ul className='children'>
                        <li className='comment'>
                          <article className='comment-body'>
                            <div className='comment-author'>
                              <img alt src={Avatar2} className='avatar' />
                            </div>
                            {/* .comment-author */}
                            <div className='comment-content'>
                              <h4 className='fn'>
                                <a href='#'>Bailey Wonger</a>
                              </h4>
                              <div className='comment-metadata'>
                                <a href='#'>July 3, 2018 at 2:14 pm</a>
                              </div>
                              {/* .comment-metadata */}
                              <p>
                                My lady mush hanky panky youn delinquent lurgy
                                the little rotter.?
                              </p>
                              <a
                                rel='nofollow'
                                className='comment-reply-link'
                                href='#'>
                                <i className='ei ei-arrow_back' />
                                Reply
                              </a>
                            </div>
                            {/* .comment-content */}
                          </article>
                          {/* .comment-body */}
                        </li>
                        {/* #comment-## */}
                        <li className='comment'>
                          <article className='comment-body'>
                            <div className='comment-author'>
                              <img alt src={Avatar2} className='avatar' />
                            </div>
                            {/* .comment-author */}
                            <div className='comment-content'>
                              <h4 className='fn'>
                                <a href='#'>Bailey Wonger</a>
                              </h4>
                              <div className='comment-metadata'>
                                <a href='#'>July 3, 2018 at 2:14 pm</a>
                              </div>
                              {/* .comment-metadata */}
                              <p>
                                My lady mush hanky panky youn delinquent lurgy
                                the little rotter.?
                              </p>
                              <a
                                rel='nofollow'
                                className='comment-reply-link'
                                href='#'>
                                <i className='ei ei-arrow_back' />
                                Reply
                              </a>
                            </div>
                            {/* .comment-content */}
                          </article>
                          {/* .comment-body */}
                        </li>
                        {/* #comment-## */}
                      </ul>
                      {/* .children */}
                    </li>
                    {/* #comment-## */}
                  </ul>
                  <div id='respond' className='comment-respond'>
                    <h3 id='reply-title' className='comment-reply-title'>
                      Leave a Comment
                    </h3>
                    <form action='#' className='comment-form'>
                      <p className='comment-form-author'>
                        <input
                          placeholder='Name'
                          id='author'
                          name='author'
                          type='text'
                          // defaultValue
                          size={30}
                        />
                      </p>
                      <p className='comment-form-email'>
                        <input
                          placeholder='Email'
                          id='email'
                          name='email'
                          type='text'
                          // defaultValue
                          size={30}
                        />
                      </p>
                      <p className='comment-form-comment'>
                        <textarea
                          placeholder='Comment'
                          id='comment'
                          name='comment'
                          cols={45}
                          rows={6}
                          aria-required='true'
                          // defaultValue={''}
                        />
                      </p>
                      <div className='form-footer'>
                        <div className='condition'>
                          <input
                            className='styled-checkbox'
                            id='styled-checkbox-1'
                            type='checkbox'
                            defaultValue='value1'
                          />
                          <label htmlFor='styled-checkbox-1' />
                          <span>
                            I agree that my submitted data is being collected
                            and stored.
                          </span>
                        </div>
                        <button type='submit' className='submit-btn pix-btn'>
                          Submit
                        </button>
                      </div>
                      {/* /.form-footer */}
                    </form>
                  </div>
                  {/* #respond */}
                </div>
              </div>
            </div>
            {/* /.container */}
          </div>
          {/* /.comment-area */}
        </section>
        {/* /.blog-single */}
      </div>
    </Layout>
  );
};

export default BlogView;
