/** @format */

import React from 'react';

const Footer = () => {
  return (
    <>
      <footer id='footer'>
        <div className='container'>
          <div className='footer-inner wow pixFadeUp'>
            <div className='row'>
              <div className='col-lg-3 col-md-6'>
                <div className='widget footer-widget'>
                  <h3 className='widget-title'>Company</h3>
                  <ul className='footer-menu'>
                    <li>
                      <a href='#'>Features</a>
                    </li>
                    <li>
                      <a href='#'>Dashboard &amp; Tool</a>
                    </li>
                    <li>
                      <a href='#'>Our Portfolio</a>
                    </li>
                    <li>
                      <a href='#'>About Us</a>
                    </li>
                    <li>
                      <a href='#'>Get In Touch</a>
                    </li>
                  </ul>
                </div>
                {/* /.widget footer-widget */}
              </div>
              {/* /.col-lg-3 col-md-6 */}
              <div className='col-lg-3 col-md-6'>
                <div className='widget footer-widget'>
                  <h3 className='widget-title'>Services</h3>
                  <ul className='footer-menu'>
                    <li>
                      <a href='#'>Features</a>
                    </li>
                    <li>
                      <a href='#'>Dashboard &amp; Tool</a>
                    </li>
                    <li>
                      <a href='#'>Our Portfolio</a>
                    </li>
                    <li>
                      <a href='#'>About Us</a>
                    </li>
                    <li>
                      <a href='#'>Get In Touch</a>
                    </li>
                  </ul>
                </div>
                {/* /.widget footer-widget */}
              </div>
              {/* /.col-lg-3 col-md-6 */}
              <div className='col-lg-3 col-md-6'>
                <div className='widget footer-widget'>
                  <h3 className='widget-title'>Digital Experience</h3>
                  <ul className='footer-menu'>
                    <li>
                      <a href='#'>Features</a>
                    </li>
                    <li>
                      <a href='#'>Dashboard &amp; Tool</a>
                    </li>
                    <li>
                      <a href='#'>Our Portfolio</a>
                    </li>
                    <li>
                      <a href='#'>About Us</a>
                    </li>
                    <li>
                      <a href='#'>Get In Touch</a>
                    </li>
                  </ul>
                </div>
                {/* /.widget footer-widget */}
              </div>
              {/* /.col-lg-3 col-md-6 */}
              <div className='col-lg-3 col-md-6'>
                <div className='widget footer-widget'>
                  <h3 className='widget-title'>Our Address</h3>
                  <p>
                    46 Eucalyptus Drive, Maidstone, Melbourne, Victoria 3012
                  </p>
                  <p>Email: info@techbae.com.au</p>
                  <p>Mobile: 0427497042</p>
                  <ul className='footer-social-link'>
                    <li>
                      <a href='#'>
                        <i className='fab fa-facebook-f' />
                      </a>
                    </li>
                    <li>
                      <a href='#'>
                        <i className='fab fa-twitter' />
                      </a>
                    </li>
                    <li>
                      <a href='#'>
                        <i className='fab fa-google-plus-g' />
                      </a>
                    </li>
                    <li>
                      <a href='#'>
                        <i className='fab fa-linkedin-in' />
                      </a>
                    </li>
                  </ul>
                </div>
                {/* /.widget footer-widget */}
              </div>
              {/* /.col-lg-3 col-md-6 */}
            </div>
            {/* /.row */}
          </div>
          {/* /.footer-inner */}
          <div className='site-info'>
            <div className='copyright'>
              <p>
                © 2021 All Rights Reserved Design by{' '}
                <a href='https://techbae.com.au/' target='_blank'>
                  Techbae Team
                </a>
              </p>
            </div>
            <ul className='site-info-menu'>
              <li>
                <a href='#'>Privacy &amp; Policy.</a>
              </li>
              <li>
                <a href='#'>Faq.</a>
              </li>
              <li>
                <a href='#'>Terms.</a>
              </li>
            </ul>
          </div>
          {/* /.site-info */}
        </div>
        {/* /.container */}
      </footer>
    </>
  );
};

export default Footer;
