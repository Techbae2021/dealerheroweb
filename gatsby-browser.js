/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/browser-apis/
 *
 * @format
 */

// You can delete this file if you're not using it
// import React from "react"
import './src/dependencies/bootstrap/css/bootstrap.min.css';
import './src/assets/css/app.css';

import './src/chatBoot/css/animate.css';
import './src/chatBoot/css/slick.css';
import './src/chatBoot/css/materialdesignicons.min.css';
import './src/chatBoot/css/line-awesome.min.css';
import './src/chatBoot/css/style.css';
// import './src/chatBoot/css/rtl-style.css';
import './src/chatBoot/css/colors/default-color.css';

import './src/dependencies/components-elegant-icons/css/elegant-icons.min.css';
import './src/dependencies/fontawesome/css/all.min.css';
import './src/dependencies/magnific-popup/css/magnific-popup.css';
import './src/dependencies/simple-line-icons/css/simple-line-icons.css';
import './src/dependencies/swiper/css/swiper.min.css';
import './src/dependencies/wow/css/animate.css';

// import MainProvider from "./src/context/Provider"
// import jquery from "jquery"
// window.$ = window.jQuery=jquery;

//const React = require("react")

export const onInitialClientRender = () => {
  console.log('ReactDOM.render has executed');
};

export const onRouteUpdate = ({ location, prevLocation }) => {
  console.log('new pathname', location.pathname);
  console.log('old pathname', prevLocation ? prevLocation.pathname : null);
  if (location.pathname === '/') {
    //window.iniJqueryFunctions();
  }
};

// export const wrapRootElement = ({ element }) => (
//   <MainProvider>{element}</MainProvider>
// )
